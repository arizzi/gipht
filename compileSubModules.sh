echo "Configure arduino-cli and compile Arduino programm ..."
echo " - - - - - - - - - - - - - - - "

cd arduino-cli/
./install.sh
./bin/arduino-cli core install arduino:avr
./bin/arduino-cli config init
./bin/arduino-cli config set directories.user ../KIRArduino/
./bin/arduino-cli compile -b arduino:avr:micro ../KIRArduino/
cd ..

echo "            DONE               "
echo "                               "


echo "Compile power_supply package ..."
echo " - - - - - - - - - - - - - - - -"

mkdir power_supply/build/
cd power_supply/build/
cmake3 ..
make -j$(nproc)

echo "            DONE               "
echo "                               "

echo "Compile Ph2_ACF package ..."
echo " - - - - - - - - - - - - - "

cd ../../
mkdir Ph2_ACF/build/
cd Ph2_ACF/
source ./setup.sh
cd build/
cmake3 .. -DPYTHON_EXECUTABLE=$(which python)
make -j$(nproc)

echo "            DONE               "
echo "                               "