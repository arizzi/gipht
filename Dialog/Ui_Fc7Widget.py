# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'Fc7Widget.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Fc7Widget(object):
    def setupUi(self, Fc7Widget):
        if not Fc7Widget.objectName():
            Fc7Widget.setObjectName(u"Fc7Widget")
        Fc7Widget.resize(540, 447)
        self.gridLayoutWidget = QWidget(Fc7Widget)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(9, 9, 521, 342))
        self.settingsLayout = QGridLayout(self.gridLayoutWidget)
        self.settingsLayout.setObjectName(u"settingsLayout")
        self.settingsLayout.setContentsMargins(0, 0, 0, 0)
        self.idLabel = QLabel(self.gridLayoutWidget)
        self.idLabel.setObjectName(u"idLabel")

        self.settingsLayout.addWidget(self.idLabel, 2, 0, 1, 1)

        self.firmwareLabel_3 = QLabel(self.gridLayoutWidget)
        self.firmwareLabel_3.setObjectName(u"firmwareLabel_3")
        self.firmwareLabel_3.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)

        self.settingsLayout.addWidget(self.firmwareLabel_3, 7, 0, 1, 1)

        self.idLineEdit = QLineEdit(self.gridLayoutWidget)
        self.idLineEdit.setObjectName(u"idLineEdit")

        self.settingsLayout.addWidget(self.idLineEdit, 2, 1, 1, 4)

        self.load2SCIC2FWButton = QPushButton(self.gridLayoutWidget)
        self.load2SCIC2FWButton.setObjectName(u"load2SCIC2FWButton")

        self.settingsLayout.addWidget(self.load2SCIC2FWButton, 7, 2, 1, 1)

        self.firmwareLabel = QLabel(self.gridLayoutWidget)
        self.firmwareLabel.setObjectName(u"firmwareLabel")
        self.firmwareLabel.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)

        self.settingsLayout.addWidget(self.firmwareLabel, 5, 1, 1, 4)

        self.indexLabel = QLabel(self.gridLayoutWidget)
        self.indexLabel.setObjectName(u"indexLabel")

        self.settingsLayout.addWidget(self.indexLabel, 1, 4, 1, 1)

        self.loadPS10GFWButton = QPushButton(self.gridLayoutWidget)
        self.loadPS10GFWButton.setObjectName(u"loadPS10GFWButton")

        self.settingsLayout.addWidget(self.loadPS10GFWButton, 7, 4, 1, 1)

        self.closeButton = QPushButton(self.gridLayoutWidget)
        self.closeButton.setObjectName(u"closeButton")

        self.settingsLayout.addWidget(self.closeButton, 0, 0, 1, 1)

        self.deviceLabel_2 = QLabel(self.gridLayoutWidget)
        self.deviceLabel_2.setObjectName(u"deviceLabel_2")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.deviceLabel_2.sizePolicy().hasHeightForWidth())
        self.deviceLabel_2.setSizePolicy(sizePolicy)

        self.settingsLayout.addWidget(self.deviceLabel_2, 1, 0, 1, 1)

        self.searchFc7sButton = QPushButton(self.gridLayoutWidget)
        self.searchFc7sButton.setObjectName(u"searchFc7sButton")

        self.settingsLayout.addWidget(self.searchFc7sButton, 3, 1, 1, 1)

        self.checkButton = QPushButton(self.gridLayoutWidget)
        self.checkButton.setObjectName(u"checkButton")

        self.settingsLayout.addWidget(self.checkButton, 0, 1, 1, 1)

        self.loadPS5GFWButton = QPushButton(self.gridLayoutWidget)
        self.loadPS5GFWButton.setObjectName(u"loadPS5GFWButton")

        self.settingsLayout.addWidget(self.loadPS5GFWButton, 7, 3, 1, 1)

        self.ipLineEdit = QLineEdit(self.gridLayoutWidget)
        self.ipLineEdit.setObjectName(u"ipLineEdit")

        self.settingsLayout.addWidget(self.ipLineEdit, 4, 1, 1, 4)

        self.firmwareComboBox = QComboBox(self.gridLayoutWidget)
        self.firmwareComboBox.setObjectName(u"firmwareComboBox")

        self.settingsLayout.addWidget(self.firmwareComboBox, 6, 1, 1, 4)

        self.saveButton = QPushButton(self.gridLayoutWidget)
        self.saveButton.setObjectName(u"saveButton")

        self.settingsLayout.addWidget(self.saveButton, 0, 2, 1, 3)

        self.ipAddressLabel = QLabel(self.gridLayoutWidget)
        self.ipAddressLabel.setObjectName(u"ipAddressLabel")

        self.settingsLayout.addWidget(self.ipAddressLabel, 3, 0, 1, 1)

        self.index = QLabel(self.gridLayoutWidget)
        self.index.setObjectName(u"index")
        self.index.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.settingsLayout.addWidget(self.index, 1, 3, 1, 1)

        self.firmwareLabel_2 = QLabel(self.gridLayoutWidget)
        self.firmwareLabel_2.setObjectName(u"firmwareLabel_2")
        self.firmwareLabel_2.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)

        self.settingsLayout.addWidget(self.firmwareLabel_2, 5, 0, 1, 1)

        self.deviceLabel = QLabel(self.gridLayoutWidget)
        self.deviceLabel.setObjectName(u"deviceLabel")

        self.settingsLayout.addWidget(self.deviceLabel, 1, 1, 1, 1)

        self.load2SCIC1FWButton = QPushButton(self.gridLayoutWidget)
        self.load2SCIC1FWButton.setObjectName(u"load2SCIC1FWButton")

        self.settingsLayout.addWidget(self.load2SCIC1FWButton, 7, 1, 1, 1)

        self.ipAddressComboBox = QComboBox(self.gridLayoutWidget)
        self.ipAddressComboBox.setObjectName(u"ipAddressComboBox")

        self.settingsLayout.addWidget(self.ipAddressComboBox, 3, 2, 1, 3)

        self.load2SKICKOFFFWButton = QPushButton(self.gridLayoutWidget)
        self.load2SKICKOFFFWButton.setObjectName(u"load2SKICKOFFFWButton")

        self.settingsLayout.addWidget(self.load2SKICKOFFFWButton, 8, 1, 1, 1)


        self.retranslateUi(Fc7Widget)

        QMetaObject.connectSlotsByName(Fc7Widget)
    # setupUi

    def retranslateUi(self, Fc7Widget):
        Fc7Widget.setWindowTitle(QCoreApplication.translate("Fc7Widget", u"Form", None))
        self.idLabel.setText(QCoreApplication.translate("Fc7Widget", u"ID", None))
        self.firmwareLabel_3.setText(QCoreApplication.translate("Fc7Widget", u"Load FW", None))
        self.load2SCIC2FWButton.setText(QCoreApplication.translate("Fc7Widget", u"2S (CIC2)", None))
        self.firmwareLabel.setText(QCoreApplication.translate("Fc7Widget", u"TextLabel", None))
        self.indexLabel.setText(QCoreApplication.translate("Fc7Widget", u"0", None))
        self.loadPS10GFWButton.setText(QCoreApplication.translate("Fc7Widget", u"PS (10G)", None))
        self.closeButton.setText(QCoreApplication.translate("Fc7Widget", u"Close", None))
        self.deviceLabel_2.setText(QCoreApplication.translate("Fc7Widget", u"Device", None))
        self.searchFc7sButton.setText(QCoreApplication.translate("Fc7Widget", u"Search FC7s", None))
        self.checkButton.setText(QCoreApplication.translate("Fc7Widget", u"Check", None))
        self.loadPS5GFWButton.setText(QCoreApplication.translate("Fc7Widget", u"PS (5G)", None))
        self.saveButton.setText(QCoreApplication.translate("Fc7Widget", u"Save", None))
        self.ipAddressLabel.setText(QCoreApplication.translate("Fc7Widget", u"IP Address", None))
        self.index.setText(QCoreApplication.translate("Fc7Widget", u"Index", None))
        self.firmwareLabel_2.setText(QCoreApplication.translate("Fc7Widget", u"Firmware", None))
        self.deviceLabel.setText(QCoreApplication.translate("Fc7Widget", u"FC7", None))
        self.load2SCIC1FWButton.setText(QCoreApplication.translate("Fc7Widget", u"2S (CIC1)", None))
        self.load2SKICKOFFFWButton.setText(QCoreApplication.translate("Fc7Widget", u"2S KICKOFF", None))
    # retranslateUi

