# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'EthernetWidget.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_EthernetWidget(object):
    def setupUi(self, EthernetWidget):
        if not EthernetWidget.objectName():
            EthernetWidget.setObjectName(u"EthernetWidget")
        EthernetWidget.resize(520, 422)
        self.gridLayoutWidget = QWidget(EthernetWidget)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(9, 9, 501, 122))
        self.settingsLayout = QGridLayout(self.gridLayoutWidget)
        self.settingsLayout.setObjectName(u"settingsLayout")
        self.settingsLayout.setContentsMargins(0, 0, 0, 0)
        self.label_6 = QLabel(self.gridLayoutWidget)
        self.label_6.setObjectName(u"label_6")

        self.settingsLayout.addWidget(self.label_6, 3, 0, 1, 1)

        self.label_4 = QLabel(self.gridLayoutWidget)
        self.label_4.setObjectName(u"label_4")

        self.settingsLayout.addWidget(self.label_4, 0, 0, 1, 1)

        self.ipAddressLineEdit = QLineEdit(self.gridLayoutWidget)
        self.ipAddressLineEdit.setObjectName(u"ipAddressLineEdit")

        self.settingsLayout.addWidget(self.ipAddressLineEdit, 0, 1, 1, 2)

        self.portLineEdit = QLineEdit(self.gridLayoutWidget)
        self.portLineEdit.setObjectName(u"portLineEdit")

        self.settingsLayout.addWidget(self.portLineEdit, 1, 1, 2, 2)

        self.label_5 = QLabel(self.gridLayoutWidget)
        self.label_5.setObjectName(u"label_5")

        self.settingsLayout.addWidget(self.label_5, 1, 0, 2, 1)

        self.timeoutLineEdit = QLineEdit(self.gridLayoutWidget)
        self.timeoutLineEdit.setObjectName(u"timeoutLineEdit")

        self.settingsLayout.addWidget(self.timeoutLineEdit, 3, 1, 1, 2)


        self.retranslateUi(EthernetWidget)

        QMetaObject.connectSlotsByName(EthernetWidget)
    # setupUi

    def retranslateUi(self, EthernetWidget):
        EthernetWidget.setWindowTitle(QCoreApplication.translate("EthernetWidget", u"Form", None))
        self.label_6.setText(QCoreApplication.translate("EthernetWidget", u"Timeout", None))
        self.label_4.setText(QCoreApplication.translate("EthernetWidget", u"IP Address", None))
        self.label_5.setText(QCoreApplication.translate("EthernetWidget", u"Port", None))
    # retranslateUi

