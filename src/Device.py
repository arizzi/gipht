#------------------------------------------------
#+++++++++++++Device.py++++++++++++++++++++++
#Created by Stefan Maier            05.11.2020
#Last modified by Stefan Maier      05.11.2020
#------------------------------------------------

from src.TCP_Interface import TCP_Interface
from src.Ph2_ACF_Interface import Ph2_ACF_Interface
from src.XMLManipulator import XMLManipulator
import serial, os
import serial.tools.list_ports
from PySide2.QtCore import Signal, QObject
from datetime import datetime

from Dialog.ConfigWidgets import *

class Device( QObject ):
    configUpdate    = Signal( object )  #There was an update of the configs
    #                         command   value   channel (optional)
    deviceCommand   = Signal( object, object, object, object )  #Send to the controller so that the controller can start the __threaded__ execution

    taskDone        = Signal( object )  #Current task finished
    statusUpdate    = Signal( object )  #The status of the device changed
    progressUpdate  = Signal( object )  #There is progress on the current task
    logUpdate       = Signal( object )  #New logs are available
    data            = Signal( object )  #Data

    possibleConnections = []
    def __init__( self, pIndex, pConfig, pDevicePackageOrPh2AcfFolder ):
        super(Device, self).__init__(  )
        #If not done before fill Type entry
        pConfig["Type"] = pConfig.get("Type","Device")
        self.xmlFile = os.getcwd() + "/settings/Devices/" + pConfig.get("Type","Device")  + "_" + pConfig.get("ID",str( pIndex ) ) + ".xml"

        self.index  = pIndex
        self.folder   = pDevicePackageOrPh2AcfFolder 
        self.config = {}

        self.type           = pConfig["Type"]

        self.status         = "Unknown"
        self.task           = ""
        self.lastReadout    = datetime.now()  #Stores the timestamp of the last readout. Comparing this to current time allows to avoid too much readout by the monitor

        self.serverOrPipeStatus = False

        self.progress   = 0

        self.log        = [ "New device: " + self.type + " (" + str(self.index) + ")" ]
        
        self.hwInterface = None

        self.changeConfigs(pConfig)

        self.storeMonitorData = None

        print("GIPHT:\tNew " + self.config["Type"] + " (index: " + str(self.index) + ")")

    #Changes the widget when the device is generated or the connection type was changed
    def changeAndConnectWidget( self ):
        #del self.configWidget
        if self.type in ["Arduino"]:
            self.configWidget = ArduinoWidget( self )
        elif self.type in ["FC7"]:
            self.configWidget = Fc7Widget( self )
        elif self.type in ["PowerSupply"]:
            self.configWidget = PowerSupplyWidget( self )
        else:
            self.configWidget = ConfigWidget( self )    #Case should not be possible

        self.configWidget.configChanged.connect(self.changeConfigs)
        self.configWidget.commandInput.connect(partial ( self.deviceCommand.emit, self.index ) )

        #Connect the OWN signals to the widget to keep it updated
        self.statusUpdate.connect(self.configWidget.update)

    def createInterface( self, pConfigFile):
        if self.hwInterface is not None:
            self.hwInterface.update.disconnect()
        if self.type in ["FC7"]:
            print("GIPHT:\tCreate Ph2_ACF Interface for type " + self.type + " ID: " + self.config.get("ID", "Unknown") )
            self.hwInterface = Ph2_ACF_Interface(self.folder, self.type, self.index)
        if self.type in ["Arduino", "PowerSupply"]:
            print("GIPHT:\tCreate TCP Interface for type " + self.type + " ID: " + self.config.get("ID","-") )

            if self.config["Connection"] == "Serial" and "Port" in self.config.keys():
                try:
                    print("GIPHT:\tMake sure the serial ports are free by opening and closing them")
                    port = serial.Serial(  port=self.config["Port"],
                                            baudrate=self.config["BaudRate"],
                                            parity=serial.PARITY_NONE,
                                            stopbits=serial.STOPBITS_ONE,
                                            bytesize=serial.EIGHTBITS,
                                            xonxoff= True)
                    #time.sleep(0.15)
                    port.close()
                except:
                    print("GIPHT:\t" + self.config["Port"] + " nothing attached")
            self.hwInterface = TCP_Interface( self.index, self.folder, pConfigFile, str(self.config.get("LaunchServer", True)), self.config.get("ExternalIPPort", "") )
        
        self.hwInterface.update.connect( self.handleInterfaceUpdate )
        self.connectDataInterface()

    def isIdle( self ):
        return self.status in ["Idle"]

    def execute( self, pCmd, pPrintLog = True ):
        self.status = "Busy"
        self.statusUpdate.emit(self)
        self.hwInterface.executeCommand(pCmd, pPrintLog)

    def handleInterfaceUpdate( self, pType, pData ):
        if self.status != "ReadingData":
            self.status = "ReadingData"
            self.statusUpdate.emit(self)

        if pType == "warn":
            warning = " |W|  \t" + pData
            self.log.append( warning )
            self.statusUpdate.emit( self )
        elif pType == "err":
            error = " |E|  \t" + pData
            self.log.append( error )
            self.statusUpdate.emit( self )
        elif pType == "msg":
            log = " |I|  \t" + pData
            self.log.append( log )
            self.logUpdate.emit( log )
        elif pType == "stat":
            status = " |S|  \t" + pData
            self.log.append( status )
            self.task = pData     #Switch from Ph2_ACF status to currentTask
            self.statusUpdate.emit( self )
        elif pType == "prog":
            self.progress = int( pData )
            self.progressUpdate.emit( self )
        elif pType == "data":
            self.data.emit( pData )
            self.status = "Idle"
            self.statusUpdate.emit(self)
            self.taskDone.emit(self)
        elif pType == "noData":
            self.status = "NoData"
            print("GIPHT:\tNo data from " + self.config.get("Type","") + " " + self.config.get("ID","") + " -  stop readout")
            self.statusUpdate.emit(self)
            self.taskDone.emit(self)
        elif pType == "finished":           #Send by the Ph2_ACF interface when the pipe is empty (=noData) check if the device WAS reading data, if yes, set status to idle
            if self.status == "ReadingData":
                self.status = "Idle"
                self.statusUpdate.emit(self)
                self.taskDone.emit(self)
            elif self.status == "Busy": #This means there were not data transmitted. Device is not connective call no data!
                pass
                #self.manageNoData()
            else:
                self.statusUpdate.emit(self)
                self.taskDone.emit(self)
        

    def stopTask ( self ):     
        if self.status not in ["Unknown","Idle"]:
            self.hwInterface.stopTask()

    #Central method called if a config is changed "from the outside" a Signal is emitted to tell the window that samething changed
    def changeConfigs( self, pDict):
        for config, value in pDict.items():
            self.config[config] = value
            if config == "Channels":
                self.generateChannels(value)
            elif config == "LEDs":
                #print("Update leds")
                self.updateLeds(value)

        self.configUpdate.emit( self )
        self.updateXmlFile()

        print("GIPHT:\tConfig Change on device, index: " + str(self.index) + "\t" + str(self.config))

    def updateXmlFile( self ):
        print("GIPHT:\tUpdate XML file for device " + self.config.get("Type","Device") + " "+ self.config.get("ID",str( self.index) ) )
        xmlCreator = XMLManipulator()
        xmlCreator.updateOrCreateDeviceFile( self , self.xmlFile)


    def printConfig( self ):
        print( str ( self.config ))

    #Returns all connected USB serial ports
    def getSerialPorts( self ):
        #Serial Port
        ports = serial.tools.list_ports.comports()

        availablePorts = []
        for port, desc, hwid in sorted(ports):
            availablePorts.append(str(port))
        return availablePorts

    def decodeStatus( self, pStatus ):
        statusList = pStatus.split(',')

        timestamp = None
        statusDict = {}


        splitTimestamp = statusList[0].split(':')
        timestamp = splitTimestamp[1] + ":" + splitTimestamp[2] + ":" + splitTimestamp[3]

        for entry in statusList[1:]:
            device = entry.split('_')[0]
            statusDict[device] = {}

        for entry in statusList[1:]:
            device = entry.split('_')[0]
            channel = entry.split('_')[1]
            statusDict[device][channel] = {}

        for entry in statusList[1:]:
            device = entry.split('_')[0]
            channel = entry.split('_')[1]
            meas, value = entry.split('_')[2].split(':')
            statusDict[device][channel][meas]=value

        return timestamp, statusDict