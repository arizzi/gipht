from PySide2.QtCore import Signal, QObject
import os, subprocess, time, datetime, socket, ctypes

BUFFER_SIZE = 65535


class TCP_Interface(QObject):
    update = Signal ( object , object )

    def __init__( self,  pIndex, pPackageFolder, pConfigFile, pLaunchServer, pExternalIPPort ):
        super(TCP_Interface, self).__init__(  )
        print("GIPHT:\tNew TCP Interface")
        self.running = False
        self.index = pIndex
        self.packageFolder = pPackageFolder
        self.configFile = pConfigFile
        self.launch = pLaunchServer == "True"
        self.externalIPPort = pExternalIPPort

        self.reset()


    def reset( self ):
        self.server = TCPServer(self.index, self.launch, self.packageFolder )
        self.server.start(self.configFile)
        time.sleep(0.5)
        if self.launch:
            self.client = TCPClient(self.index, "")
        else:
            self.client = TCPClient(self.index, self.externalIPPort)

        self.client.connectClient()
        self.client.tcpAnswer.connect(self.handleAnswer)
        self.client.tcpBroken.connect(self.handleBrokenConnection)
        if self.server.checkDeviceSubProcess():
            self.running = True
        
    def executeCommand( self , pCmd, pPrintLog = False):
        self.client.sendAndReceivePacket(pCmd)

    def handleAnswer( self, pAnswer ):
        if pAnswer is not None:
            self.update.emit("data", pAnswer)
        else:
            self.update.emit("noData", None)

    def handleBrokenConnection( self ):
        print("GIPHT:\tTry to reconnect TCP Interface - Re-initialize TCP interface")
        self.running = False
        del self.server
        del self.client
        self.reset()

    #To be defined    
    def stopTask( self ):
        pass



class TCPServer():
    def __init__( self, pIndex, pLaunchServer, pFolder = None  ):
        print("GIPHT:\tNew TCP Server")
        self.index = pIndex
        self.launch = pLaunchServer
        self.folder = pFolder
        self.config = None
        self.serverProcess = None
        self.logFile = open("logs/log" + str(self.index) + ".txt", "w+")
        self.pid = None
    def start( self , pConfigFile ) :
        self.config = pConfigFile
        if self.folder is not None:
            print("GIPHT:\tTCP Server: Start TCP Device Server at " + self.folder + " with config file " + self.config)
            if not self.isRunning():
                if self.launch:
                    cmd = "source setup.sh && ./bin/PowerSupplyController -c " + self.config + " -p " + str( 7010 + self.index)
                    self.serverProcess = subprocess.Popen(cmd, stdout=self.logFile, stderr=subprocess.PIPE, encoding = 'UTF-8', shell= True, executable="/bin/bash", cwd=self.folder)
                    #self.serverProcess = subprocess.Popen(cmd, shell=True,executable="/bin/bash", cwd="/home/readout/Stefan/GUI/gipht/power_supply/")
                    self.pid = self.serverProcess.pid
                else:
                    print("GIPHT:\tServer started externally")
                return True
            else:
                print("GIPHT:\tTCP Device Server already running")
                return False
        else:
            print("GIPHT:\tNo folder given for TCP Device Server, abort!")
            return False

    def checkDeviceSubProcess ( self ):
        search = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
        output, error = search.communicate()
        target_process = "PowerSupplyCont"

        for line in output.splitlines():
            #print(str(line))
            if target_process in str(line):
                pid = int(line.split(None, 1)[0])
                #print("PID:" + str(pid))


        if self.serverProcess is not None:
            response = self.serverProcess.poll()
            #print( "Response: " + str( response) )
            print("GIPHT:\tCheck subprocess of device " + self.config + ": is running")
            return  response is None
        else:
            if self.launch:
                print("GIPHT:\tCheck subprocess of device " + self.config + ": is not running")
                return False   
            else:
                return True

    def killAnyPowerSupplyProcess( self ):
        search = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
        output, error = search.communicate()
        target_process = "PowerSupplyCont"

        for line in output.splitlines():
            if target_process in str(line):
                pid = int(line.split(None, 1)[0])

                os.kill(pid, 9)
                return True
        return False
    def stop ( self ):
        print("TCP Server: Kill TCP Device Server at " + self.folder + " with config file " + self.config)

        return self.killAnyPowerSupplyProcess()

    def isRunning( self):
        return self.checkDeviceSubProcess()


class TCPClient(QObject):
    tcpAnswer = Signal( object )
    tcpBroken = Signal ( )
    def __init__( self, pIndex, pExternalIPPort):
        super(TCPClient, self).__init__(  )
        self.index = pIndex
        self.externalIPPort = pExternalIPPort
        print("GIPHT:\tNew TCP Client " + str( self.index ))
        
        self.fPackerNumber = 0
        self.fClientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.lastPoll = datetime.datetime.now()
        self.lastData = None
        self.running = False

    def __del__(self):
        self.running = False
        self.fClientSocket.close()

    def isRunning( self ):
        return self.running

    def connectClient ( self ):
        #self.client.connect(2,1000)
        try:
            print(self.externalIPPort)
            if self.externalIPPort != "":
                ip = self.externalIPPort.split(":")[0]
                print(ip)

                port = int(self.externalIPPort.split(":")[1])
                print(port)

                self.fClientSocket.connect((ip, port))
            else:
                self.fClientSocket.connect(('127.0.0.1', 7010 + self.index))
            self.running = True
        except socket.error as e:
            #print(e)
            self.running = False
        
    def disconnectClient( self ):
        self.fClientSocket.close()

    def makeFourByteArray(self, integer):
        integerByte0 = ctypes.c_ubyte((integer >>  0) & 0xff)
        integerByte1 = ctypes.c_ubyte((integer >>  8) & 0xff)
        integerByte2 = ctypes.c_ubyte((integer >> 16) & 0xff)
        integerByte3 = ctypes.c_ubyte((integer >> 24) & 0xff)
        integerByte = bytearray(integerByte3) + bytearray(integerByte2) + bytearray(integerByte1) + bytearray(integerByte0)
        return integerByte

    def encodePacket(self, message, packetNumber):
        fullMessage = self.makeFourByteArray(len(message)+8) + self.makeFourByteArray(packetNumber) + bytearray(message, "utf-8")
        return fullMessage
    
    def decodePacket(self, message):
        return message[8:].decode()

    def sendAndReceivePacket(self, message):
        if self.running:
            #print(str(self.index) + "\t" +message)
            try:
                self.fClientSocket.send(self.encodePacket(message, self.fPackerNumber))
                self.fPackerNumber = self.fPackerNumber +1
                data = self.fClientSocket.recv(BUFFER_SIZE)
                #print(data)
                result = self.decodePacket(data)
                #print(result)
                self.tcpAnswer.emit(result)
            except BrokenPipeError:
                print("GIPHT:\tTCP Broken Pipe error!")
                self.tcpBroken.emit()

        else:
            print("GIPHT:\tTCP Socket not connected!")