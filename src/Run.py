import os, yaml

class RunItem():
    def __init__( self, pSummaryFile ):
        self.summaryFile = pSummaryFile
        self.dataSetComment = None
        self.runInfo   = None
        self.dataFiles = [] # {"Name: ": "", "Uploaded": True/False, "DBFile": ""}

        with open(self.summaryFile, 'r') as f:
            summary = yaml.safe_load(f)
            self.runInfo = summary["Info"]
            self.dataFiles = summary["DataFiles"]
        
    def updateSummaryFile( self ):
        summary = {"Info": self.runInfo, "DataFiles": self.dataFiles}
        with open(self.summaryFile, "w") as f:
            yaml.dump(summary,f)
        


class Run():
    def __init__( self, pRunInfo = None ):
        self.runInfo        = pRunInfo
        self.directory      = None
        self.comment        = None
        self.dataSetComment = None
        self.runItems       = []        #List of RunItems (like IV and Module Test)

    def extendRunItems( self, pRunItem ):
        self.runItems.append(pRunItem)

    def generateRunDataFromFolder( self, pFolder ):
        files = os.listdir(pFolder)
        print("GIPHT:\tSearch for summary files in " + pFolder)
        for file in files:
            if "summary" in file:
                print("GIPHT:\tFound measurement: " + file)
                newRunItem = RunItem(pFolder + "/" + file)
                self.runInfo = newRunItem.runInfo
                self.runItems.append(newRunItem)


    def updateRunItems( self ):
        for runItem in self.runItems:
            runItem.updateSummaryFile()
