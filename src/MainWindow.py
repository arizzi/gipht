#------------------------------------------------
#+++++++++++++MainWindow.py++++++++++++++++++++++
#Created by Stefan Maier            01.04.2020
#Last modified by Stefan Maier      13.08.2020
#------------------------------------------------

#                           Todos
# - More tooltips
# - Progress bar list unnecessary, tree widget already stores the objects
# - Add a how to page


import re, os
from functools import partial

from Ui_MainWindow import Ui_MainWindow

from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *


class MainWindow(QMainWindow):

    deviceCommand       = Signal( object, object, object, object )          #Device, Command, Value = None, ChannelIndex = None
    taskStopSignal      = Signal( object )

    windowClosed        = Signal()


    #Colorcodes to clone the Ph2_ACF output in the gipht console
    colorCodes = {  "\u001b[31m":QColor(187 ,0      ,0  ),  #Red
                    "\u001b[32m":QColor(0   ,187    ,0  ),  #Green
                    "\u001b[33m":QColor(187 ,187    ,0  ),  #Yellow
                    "\u001b[34m":QColor(36  ,114    ,200 ), #Blue
                    "\u001b[35m":QColor(187 ,0      ,187),  #Magenta
                    "\u001b[36m":QColor(0   ,187    ,187)}  #Cyan

    def __init__( self ):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.slotLayout.addStretch()

        self.ui.checkControlhub.setToolTip("Check if the controlhub for TCP connection is running")
        self.ui.checkPh2Acf.setToolTip("Check if the set Ph2_ACF is responsive")
        self.ui.ph2AcfFolderLineEdit.setToolTip("Ph2_ACF folder")

        taskHeader = QTreeWidgetItem(["No.","Type","Status","Stop"])
        self.ui.taskTreeWidget.header().setStretchLastSection(False)
        self.ui.taskTreeWidget.setHeaderItem(taskHeader)
        self.ui.taskTreeWidget.header().setSectionResizeMode(2, QHeaderView.Stretch)
        self.ui.taskTreeWidget.setVerticalScrollBarPolicy( Qt.ScrollBarAlwaysOn)
        
        deviceHeader = QTreeWidgetItem(["No.","ID","Type","Connection","Settings","Server","Mon.","Con.","Status"])
        self.ui.deviceTreeWidget.setHeaderItem(deviceHeader)
        self.ui.deviceTreeWidget.setToolTip("This list contains all devices and their current status.\nChannels for PowerSupplies can be added/removed and their output enabled/disabled.\nVoltage and current protection can be set as well")

        resultTreeHeader = QTreeWidgetItem(["Run", "Date", "Operator","Location", "File","XML DB File","Comment","Uploaded"])
        self.ui.resultsTreeWidget.setHeaderItem(resultTreeHeader)
        self.ui.resultsTreeWidget.setToolTip("List of all measurement results")

        self.ui.vMaxEncapsulatedLineEdit.setToolTip("Maximum IV voltage encapsulated (V)")
        self.ui.vMaxEncapsulatedLineEdit.setToolTip("Maximum IV voltage encapsulated (V)")
        self.ui.vMaxUnencapsulatedLineEdit.setPlaceholderText("Maximum IV voltage encapsulated (V)")
        self.ui.vMaxUnencapsulatedLineEdit.setPlaceholderText("Maximum IV voltage encapsulated (V)")
        self.ui.vStepLineEdit.setToolTip("Voltage steps")
        self.ui.vStepLineEdit.setPlaceholderText("Voltage steps")
        self.ui.settlingTimeLineEdit.setToolTip("Settling time (ms)")
        self.ui.settlingTimeLineEdit.setPlaceholderText("Settling time (ms)")

    def closeEvent(self, event):
        self.windowClosed.emit()
    #-----------------------------------------------------------------------
    #---------------------        Update the GUI    ------------------------
    #-----------------------------------------------------------------------


    #Called by the controller after it is checked whether there is a running controlhub on the local machine
    def updateControlHubStatus ( self , pStatus ):
        if pStatus:
            self.ui.checkControlhub.setText("Controlhub ON \n Check")
            self.ui.checkControlhub.setStyleSheet("background-color: green")
        else:
            self.ui.checkControlhub.setText("Controlhub OFF \n Check")
            self.ui.checkControlhub.setStyleSheet("background-color: red")


    def updateTcpServerStatus( self, pStatus ):
        if pStatus:
            self.ui.checkTcpServerButton.setText("TCP Server running Check")
            self.ui.checkTcpServerButton.setStyleSheet("background-color: green")
        else:
            self.ui.checkTcpServerButton.setText("TCP Server not running Check")
            self.ui.checkTcpServerButton.setStyleSheet("background-color: red")

    #Called by the controller after the current Ph2_ACF folder is checked for a responsive Ph2_ACF
    def updatePh2AcfStatus ( self , pStatus, pPath ):
        if pStatus:
            self.ui.checkPh2Acf.setText("Ph2_ACF responsive \n Check")
            self.ui.checkPh2Acf.setStyleSheet("background-color: green")
        else:
            self.ui.checkPh2Acf.setText("Ph2_ACF not responsive \n Check")
            self.ui.checkPh2Acf.setStyleSheet("background-color: red")

    #This method changes settings display on the GUI. The settings are stored in a .json file from the controller
    def displayConfig ( self, pConfig, pValue ):
        print("GIPHT:\tChange display: " + str(pConfig) + ":\t" + str(pValue))
        #List to be expanded here if more settings worth storing are added...
        if pConfig == "Ph2_ACF_Folder":
            self.ui.ph2AcfFolderLineEdit.setText(pValue)
            self.updatePh2AcfStatus( False, pValue )
        elif pConfig == "Device_Package_Folder":
            self.ui.devicePackageFolderLineEdit.setText(pValue)
            #self.updatePh2AcfStatus( False, pValue )
        elif pConfig == "Default2SHwFile":
            self.ui.default2SXmlLineEdit.setText(pValue)
        elif pConfig == "DefaultPSHwFile":
            self.ui.defaultPSXmlLineEdit.setText(pValue)
        elif pConfig == "LocalRunNumber":
            self.ui.runNumberLabelValue.setText(str(pValue))
        elif pConfig == "Result_Folder":
            self.ui.resultFolderLineEdit.setText(pValue)
        elif pConfig == "IV_Readouts":
            self.ui.readoutsSpinBox.blockSignals(True)
            self.ui.readoutsSpinBox.setValue(int(pValue))
            self.ui.readoutsSpinBox.blockSignals(False)
        elif pConfig == "Settling_Time":
            self.ui.settlingTimeLineEdit.setText(str(pValue))
        elif pConfig == "IV_Max_Encapsulated":
            self.ui.vMaxEncapsulatedLineEdit.setText(str(pValue))
        elif pConfig == "IV_Max_Unencapsulated":
            self.ui.vMaxUnencapsulatedLineEdit.setText(str(pValue))
        elif pConfig == "IV_Step":
            self.ui.vStepLineEdit.setText(str(pValue))
        elif pConfig == "HV_Test_Unencapsulated":
            self.ui.HVModuleTestUnencapsulatedLineEdit.setText(str(pValue))
        elif pConfig == "HV_Test_Encapsulated":
            self.ui.HVModuleTestEncapsulatedLineEdit.setText(str(pValue))
        elif pConfig == "LV_Test":
            self.ui.LVModuleTestLineEdit.setText(str(pValue))
        elif pConfig == "AutoCheck_Startup":
            self.ui.autoCheckStartCheckBox.blockSignals(True)
            if int(pValue)>0:
                self.ui.autoCheckStartCheckBox.setCheckState(Qt.Checked)
            else:
                self.ui.autoCheckStartCheckBox.setCheckState(Qt.Unchecked)
            self.ui.autoCheckStartCheckBox.blockSignals(False)
        elif pConfig == "PowerSupply_Dialog":
            self.ui.powerSupplyDialogCheckBox.blockSignals(True)
            if int(pValue)>0:
                self.ui.powerSupplyDialogCheckBox.setCheckState(Qt.Checked)
            else:
                self.ui.powerSupplyDialogCheckBox.setCheckState(Qt.Unchecked)
            self.ui.powerSupplyDialogCheckBox.blockSignals(False)
        elif pConfig == "Show_Plot":
            self.ui.showPlotCheckBox.blockSignals(True)
            if int(pValue)>0:
                self.ui.showPlotCheckBox.setCheckState(Qt.Checked)
            else:
                self.ui.showPlotCheckBox.setCheckState(Qt.Unchecked)
            self.ui.showPlotCheckBox.blockSignals(False)

        elif pConfig == "Invert_Plot":
            self.ui.invertPlotCheckBox.blockSignals(True)
            if int(pValue)>0:
                self.ui.invertPlotCheckBox.setCheckState(Qt.Checked)
            else:
                self.ui.invertPlotCheckBox.setCheckState(Qt.Unchecked)
            self.ui.invertPlotCheckBox.blockSignals(False)
        elif pConfig == "CheckDB_Startup":
            self.ui.initDbHandlerOnStartupCheckBox.blockSignals(True)
            if int(pValue)>0:
                self.ui.initDbHandlerOnStartupCheckBox.setCheckState(Qt.Checked)
            else:
                self.ui.initDbHandlerOnStartupCheckBox.setCheckState(Qt.Unchecked)
            self.ui.initDbHandlerOnStartupCheckBox.blockSignals(False)
        elif pConfig == "CheckModuleWithDB":
            self.ui.useDbToCheckModulesButton.blockSignals(True)
            if int(pValue)>0:
                self.ui.useDbToCheckModulesButton.setCheckState(Qt.Checked)
            else:
                self.ui.useDbToCheckModulesButton.setCheckState(Qt.Unchecked)
            self.ui.useDbToCheckModulesButton.blockSignals(False)
        elif pConfig == "DBWrapper_BaseURL":
            self.ui.baseUrlLineEdit.setText(str(pValue))
        elif pConfig == "DBWrapper_UploadURL":
            self.ui.uploadUrlLineEdit.setText(str(pValue))
        elif pConfig == "Location":
            self.ui.locationComboBox.blockSignals(True)
            index = self.ui.locationComboBox.findText ( str ( pValue ), Qt.MatchFixedString)
            if index >= 0:
                self.ui.locationComboBox.setCurrentIndex(index)
            self.ui.locationComboBox.blockSignals(False)
        elif pConfig == "Operator":
            self.ui.operatorComboBox.blockSignals(True)
            index = self.ui.operatorComboBox.findText ( str ( pValue ), Qt.MatchFixedString)
            if index >= 0:
                self.ui.operatorComboBox.setCurrentIndex(index)
            self.ui.operatorComboBox.blockSignals(False)
        elif pConfig == "Threshold":
            self.ui.thresholdLineEdit.setText( str ( pValue ) )
        elif pConfig == "Latency":
            self.ui.latencyLineEdit.setText( str ( pValue ) )
        elif pConfig == "N_Events":
            self.ui.nEventsLineEdit.setText( str ( pValue ) )
        elif pConfig == "External_Trigger":
            self.ui.extTriggerCheckBox.blockSignals(True)
            if int(pValue)>0:
                self.ui.extTriggerCheckBox.setCheckState(Qt.Checked)
            else:
                self.ui.extTriggerCheckBox.setCheckState(Qt.Unchecked)
            self.ui.extTriggerCheckBox.blockSignals(False)
        elif pConfig == "CIC_Version":
            if int(pValue) == 1:
                self.ui.cic1RadioButton.setChecked(True)
            elif int(pValue) == 2:
                self.ui.cic2RadioButton.setChecked(True)
        elif pConfig == "lpGBT_Version":
            if int(pValue) == 0:
                self.ui.lpGBTv0RadioButton.setChecked(True)
            elif int(pValue) == 1:
                self.ui.lpGBTv1RadioButton.setChecked(True)
        elif pConfig == "VTRXLightOff":
            self.ui.vtrxLightOffCheckBox.blockSignals(True)
            if int(pValue)>0:
                self.ui.vtrxLightOffCheckBox.setCheckState(Qt.Checked)
            else:
                self.ui.vtrxLightOffCheckBox.setCheckState(Qt.Unchecked)
            self.ui.vtrxLightOffCheckBox.blockSignals(False)
        elif pConfig == "Expert_Mode":
            self.ui.expertModeCheckBox.blockSignals(True)
            if int(pValue)>0:
                self.ui.expertModeCheckBox.setCheckState(Qt.Checked)
            else:
                self.ui.expertModeCheckBox.setCheckState(Qt.Unchecked)
            self.ui.expertModeCheckBox.blockSignals(False)
            

    def setDbStatus(self, pStatus):
        if pStatus == "Unchecked":
            self.ui.dbStatusDisplayLabel.setText("Unchecked")
            self.ui.dbStatusDisplayLabel_2.setText("Unchecked")
            self.ui.dbStatusDisplayLabel.setStyleSheet("QLabel { color : orange; }")
            self.ui.dbStatusDisplayLabel_2.setStyleSheet("QLabel { color : orange; }")
        else:
            if pStatus:
                self.ui.dbStatusDisplayLabel.setText("Online")
                self.ui.dbStatusDisplayLabel_2.setText("Online")
                self.ui.dbStatusDisplayLabel.setStyleSheet("QLabel { color : green; }")
                self.ui.dbStatusDisplayLabel_2.setStyleSheet("QLabel { color : green; }")
            else:
                self.ui.dbStatusDisplayLabel.setText("Offline")
                self.ui.dbStatusDisplayLabel_2.setText("Offline")
                self.ui.dbStatusDisplayLabel.setStyleSheet("QLabel { color : red; }")
                self.ui.dbStatusDisplayLabel_2.setStyleSheet("QLabel { color : red; }")


    def showNotes( self, pNotes = None ):
        if pNotes:
            text = "Notes:\n"
            for note in pNotes:
                if note is not None:
                    text = text + "\n" + note
                else:
                    text = text + "\n"
            textBox = QLabel(text)
            self.ui.deviceConfigsLayout.addWidget(textBox)

    def showModuleTestSettings( self, pVisible ):
        self.ui.thresholdLabel.setVisible(pVisible)
        self.ui.thresholdLineEdit.setVisible(pVisible)
        self.ui.latencyLabel.setVisible(pVisible)
        self.ui.latencyLineEdit.setVisible(pVisible)
        self.ui.nEventsLabel.setVisible(pVisible)
        self.ui.nEventsLineEdit.setVisible(pVisible)
        self.ui.extTriggerCheckBox.setVisible(pVisible)

    def setSlotNumber( self, pNumberOfSlots):
        self.ui.slotsSpinBox.blockSignals(True)
        self.ui.slotsSpinBox.setValue(pNumberOfSlots)
        self.ui.slotsSpinBox.blockSignals(False)

    def addTask( self, pTask ):
        newTaskItem = QTreeWidgetItem(self.ui.taskTreeWidget)
        newTaskItem.setText(0,str(pTask.index))
        newTaskItem.setText(1,pTask.type)
        if "Running" in pTask.status:
            newTaskItem.setForeground( 2 , Qt.darkCyan )
        elif "Done" in pTask.status:
            newTaskItem.setForeground( 2 , Qt.darkGreen )
        else:
            newTaskItem.setForeground( 2 , Qt.blue )
        newTaskItem.setText(2,pTask.status)

        stopTaskButton = QPushButton("Stop")
        stopTaskButton.resize(150,30)
        self.ui.taskTreeWidget.setItemWidget(newTaskItem,3,stopTaskButton)
        stopTaskButton.clicked.connect (partial ( self.taskStopSignal.emit, pTask ) )

        self.ui.taskTreeWidget.verticalScrollBar().setSliderPosition(self.ui.taskTreeWidget.topLevelItemCount())

    def updateTask( self, pTask ):
        iterator = QTreeWidgetItemIterator(self.ui.taskTreeWidget)
        while iterator.value():
            item = iterator.value()
            if item.text(0) == str(pTask.index):
                if "Running" in pTask.status:
                    item.setForeground( 2 , Qt.darkCyan )
                elif "Done" in pTask.status:
                    item.setForeground( 2 , Qt.darkGreen )
                else:
                    item.setForeground( 2 , Qt.blue )
                item.setText(2,pTask.status)
            iterator += 1
        for column in range(3):
            self.ui.taskTreeWidget.resizeColumnToContents( column )

    def removeTask( self, pTask):
        iterator = QTreeWidgetItemIterator(self.ui.taskTreeWidget)
        itemCount = 0
        while iterator.value():
            item = iterator.value()
            if item.text(0) == str(pTask.index):
                self.ui.taskTreeWidget.takeTopLevelItem(itemCount)
                break
            itemCount += 1
            iterator += 1

    def emitDeviceCommand( self,  pDeviceIndex, pCommand, pValue = None, pChannelIndex = None ):
        #print("GUI emit: Index: " + str(pDeviceIndex) + "\tCommand: " + str(pCommand) + "\tValue: " + str(pValue) + "\tChannelIndex" + str(pChannelIndex))
        self.deviceCommand.emit( pDeviceIndex, pCommand, pValue, pChannelIndex )

    def clearDeviceTreeWidget( self ):
        for i in range(self.ui.deviceTreeWidget.topLevelItemCount()):
            self.ui.deviceTreeWidget.topLevelItem(0).takeChildren()
            self.ui.deviceTreeWidget.takeTopLevelItem(0)            #Crashes sometims? (segfault)
            

    def addDevice ( self , pDevice ):
        self.addDeviceToTree(pDevice)
        pDevice.statusUpdate.connect(self.updateDevice)
        pDevice.configUpdate.connect(self.updateDevice)
        pDevice.logUpdate.connect(self.appendConsole)


    def addDeviceToTree ( self, pDevice ):
        #["No.","ID","Type", "Connection","Settings","Status"]
        #Initialize the tree line representing the new Device
        newDevice = QTreeWidgetItem(self.ui.deviceTreeWidget)
        #No.
        newDevice.setText(0, "")

        #ID
        newDevice.setText(1, "" )

        #Type
        newDevice.setText(2, pDevice.config["Type"] )

        #Connection
        newDevice.setText(3, pDevice.config.get("Connection","" ))


        #Settings
        deviceSettingsButton = QPushButton("Settings")
        self.ui.deviceTreeWidget.setItemWidget(newDevice,4,deviceSettingsButton)
        deviceSettingsButton.clicked.connect( partial ( self.showDeviceConfig, pDevice ) )
        self.ui.deviceConfigsLayout.addWidget(pDevice.configWidget, 0)
        pDevice.configWidget.hide()

        #Update the line with the correct information
        if pDevice.config["Type"] == "PowerSupply":
            for channel in pDevice.channels:
                self.addChannel(pDevice, channel)

        serverBox = QCheckBox()
        serverBox.setDisabled(True)
        self.ui.deviceTreeWidget.setItemWidget(newDevice,5,serverBox)
        self.ui.deviceTreeWidget.setColumnWidth(5,30)


        monitorBox = QCheckBox()
        monitorBox.setDisabled(True)
        self.ui.deviceTreeWidget.setItemWidget(newDevice,6,monitorBox)
        monitorBox.stateChanged.connect( partial ( self.checkMonitorState, pDevice.index ) )
        self.ui.deviceTreeWidget.setColumnWidth(6,30)


        connectionSlider = QSlider(Qt.Horizontal)
        connectionSlider.setRange(0,1)
        connectionSlider.setTickInterval(1)
        self.ui.deviceTreeWidget.setItemWidget(newDevice,7,connectionSlider)
        self.ui.deviceTreeWidget.setColumnWidth(7,40)
        connectionSlider.sliderPressed.connect( partial ( self.checkConnectionBySlider, pDevice.index))
        connectionSlider.valueChanged.connect( partial ( self.checkConnectionBySlider, pDevice.index))

        self.updateDevice( pDevice )

    def checkMonitorState( self, pDeviceIndex, pValue ):
        if pValue >0:
            self.emitDeviceCommand(pDeviceIndex, "Start MonitorPoll")
        else:
            self.emitDeviceCommand(pDeviceIndex, "Stop MonitorPoll")

    def checkConnectionBySlider( self, pDeviceIndex, pValue = None):

        if pValue is not None:
            value = pValue
        else:
            pValue = self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDeviceIndex), 7).value()
            value = not pValue
            self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDeviceIndex), 7).blockSignals(True)
            self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDeviceIndex), 7).setValue(not pValue)
            self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDeviceIndex), 7).blockSignals(False)
        if value:
            self.emitDeviceCommand(pDeviceIndex, "Check")


    def removeDevice( self ):
        self.ui.deviceTreeWidget.takeTopLevelItem(self.ui.deviceTreeWidget.topLevelItemCount()-1)

    #Adds a channel subitem to the powersupply (via the index)
    def addChannel( self, pPowerSupply, pChannel ):
        channelItem = QTreeWidgetItem()
        self.ui.deviceTreeWidget.topLevelItem( pPowerSupply.index ).addChild(channelItem)
        channelItem.setText(1, str ( pChannel.index ) )
        channelItem.setText(2, "Channel" )
        statusBox = QCheckBox()
        statusBox.setDisabled(True)
        self.ui.deviceTreeWidget.setItemWidget(channelItem,7,statusBox)

        self.ui.deviceTreeWidget.expandToDepth(0)
    #Always remove the last channel in the list
    def removeChannel( self, pPowerSupply ):
        self.ui.deviceTreeWidget.topLevelItem( pPowerSupply.index).removeChild(self.ui.deviceTreeWidget.topLevelItem( pPowerSupply.index).child(self.ui.deviceTreeWidget.topLevelItem( pPowerSupply.index).childCount()-1))

    def renewChannels( self, pDevice ):
        while self.ui.deviceTreeWidget.topLevelItem(pDevice.index).childCount() > len(pDevice.channels):
            self.removeChannel( pDevice )
        while self.ui.deviceTreeWidget.topLevelItem(pDevice.index).childCount() < len(pDevice.channels):
            self.addChannel( pDevice, pDevice.channels[-1] )


    def updateDevice( self, pDevice ):
        #print(pDevice.status)
        #["No.","ID","Type", "Connection","Settings","Status"]
        #No.
        self.ui.deviceTreeWidget.topLevelItem( pDevice.index).setText(0,str ( pDevice.index ) )

        #ID
        self.ui.deviceTreeWidget.topLevelItem( pDevice.index).setText(1, pDevice.config.get("ID", "" ) )

        #Type
        self.ui.deviceTreeWidget.topLevelItem( pDevice.index).setText(2, pDevice.config.get("Type", "" ) )

        if pDevice.config["Type"] == "PowerSupply":
            #If the number of channels is not matching the dictionar they must be updated
            if self.ui.deviceTreeWidget.topLevelItem(pDevice.index).childCount() != len(pDevice.channels):
                self.renewChannels(pDevice)
            for channel in pDevice.channels:
                self.ui.deviceTreeWidget.topLevelItem( pDevice.index ).child(channel.index).setText(1,channel.config["ID"])
                if channel.status["IsOn"]== "1":
                    self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index ).child(channel.index), 7 ) .setStyleSheet("QCheckBox::indicator""{background-color: green;""}")
                elif channel.status["IsOn"] == "Unknown":
                    self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index ).child(channel.index), 7 ) .setStyleSheet("QCheckBox::indicator""{background-color: orange;""}")
                else:
                    self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index ).child(channel.index), 7 ) .setStyleSheet("QCheckBox::indicator""{background-color: red;""}")

                try:
                    v = "{:.2f}".format( float ( channel.status["Voltage"] ) ) + "V"
                    if pDevice.isHVLV() == "LV":
                        i = "{:.2f}".format( float ( channel.status["Current"] ) ) + "A"
                    else:
                        if pDevice.config["Model"] != "CAEN":
                            i = "{:.2f}".format( float ( channel.status["Current"] )*10**9 ) + "nA"
                        else:
                            i = "{:.2f}".format( float ( channel.status["Current"] ) ) + "µA"

                    status = v + " | " + i
                except ValueError:
                    status = ""
                self.ui.deviceTreeWidget.topLevelItem( pDevice.index ).child(channel.index).setText(8,status)
                #self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index) .child(channel.index), 1)

        if pDevice.type not in ["FC7"]:
            if pDevice.serverOrPipeStatus:
                self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index), 5).setStyleSheet("QCheckBox::indicator""{background-color: green;""}")
            else:
                self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index), 5).setStyleSheet("QCheckBox::indicator""{background-color: red;""}")
        self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index), 7).blockSignals(True)
        if pDevice.status in ["Idle","ReadingData"]:
            self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index), 7).setStyleSheet("QSlider::handle:horizontal {background-color: green;}")
            self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index), 7).setValue(1)

            self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index), 6).setDisabled(False)

        elif pDevice.status in ["Busy","Config"]:
            self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index), 7).setValue(1)
            self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index), 7).setStyleSheet("QSlider::handle:horizontal {background-color: orange;}")

            self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index), 6).setDisabled(False)

        else:
            self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index), 7).setValue(0)
            self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index), 7).setStyleSheet("QSlider::handle:horizontal {background-color: red;}")
            self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index), 6).setDisabled(True)

        self.ui.deviceTreeWidget.itemWidget ( self.ui.deviceTreeWidget.topLevelItem( pDevice.index), 7).blockSignals(False)

        self.ui.deviceTreeWidget.topLevelItem(pDevice.index).setText(8,pDevice.task + " - " + pDevice.status)

        #Resize
        for column in range(6):
            self.ui.deviceTreeWidget.resizeColumnToContents( column )
        
    def addSlotStatus( self, pStatusWidget ):
        self.ui.slotLayout.insertWidget(self.ui.slotLayout.count()-1, pStatusWidget )

    def removeSlotStatus ( self ):
        item = self.ui.slotLayout.takeAt(self.ui.slotLayout.count()-2)
        w = item.widget()
        if w:
            w.deleteLater()
    def clearSlots( self ):
        for i in range (self.ui.slotLayout.count() - 1):
            self.removeSlotStatus()
    
    def showDeviceConfig( self, pDevice ):
        self.hideDeviceConfigs()
        pDevice.configWidget.show()

    def hideDeviceConfigs( self ):
        for i in range ( self.ui.deviceConfigsLayout.count() ):
            self.ui.deviceConfigsLayout.itemAt(i).widget().hide()

    #Adds a new line to the gipht console. Uses the color codes from the init file for a nice print
    def appendConsole ( self, pStringToAppend ):
        #Separate the string in pieces: Search for color or font change
        #The console output of the Ph2_ACF, can be colored or bold
        stringPieces = re.split( re.compile(r'(\x1b[^m]*m)' ) , pStringToAppend )
        stringPieces = list(filter(None,stringPieces))

        #For each substring(piece) check wether is is colored or bold and append the console accordingly
        for piece in stringPieces:
            if "\u001b[1m" in piece:
                #Somehow boldness does not work -.-
                self.ui.consoleOutput.setFont(QFont("Cantarell", 7, QFont.Bold))
            elif "\u001b[0m" in piece:
                self.ui.consoleOutput.setFont(QFont("Cantarell", 7))
                self.ui.consoleOutput.setTextColor(QColor(255, 255, 255))
            elif piece in self.colorCodes:
                self.ui.consoleOutput.setTextColor(self.colorCodes[piece])
            else:
                self.ui.consoleOutput.insertPlainText(piece)
        self.ui.consoleOutput.insertPlainText("\n")
        self.ui.consoleOutput.verticalScrollBar().setValue( self.ui.consoleOutput.verticalScrollBar().maximum() )

    #Update the state of the checkboxes when the board was changed, as well as the progress bar
    def updateMeasurementInfo ( self , pMeasurements ):
        for count in range( self.ui.measurementCheckBoxes.count() ):
            box = self.ui.measurementCheckBoxes.itemAt(count).widget()
            box.blockSignals(True)
            box.setChecked(pMeasurements[box.text()])
            box.blockSignals(False)

    #Update the measurement prgress bar depending on which object calls the method
    def updateTaskProgressBar ( self , pObject ):
        self.ui.taskProgressBar.setValue( pObject.progress )
        self.ui.taskProgressBar.setFormat( pObject.task + "\t\t(" + str( pObject.progress ) + "%)" )

    #A run can contain several measurements and has its own progress bar
    def updateRunProgressBar(self , pProgress, pType):
        self.ui.runProgressBar.setValue( pProgress*100 )
        self.ui.runProgressBar.setFormat( pType + "\t\t(" + "{:.0f}".format(pProgress*100) + "%)" )


    #-----------------------------------------------------------------------
    #------------------------------- RESULTS -------------------------------
    #-----------------------------------------------------------------------

    def updateRuns( self, pRuns ):
        for i in reversed( range(self.ui.resultsTreeWidget.topLevelItemCount()) ):
            self.ui.resultsTreeWidget.takeTopLevelItem(i)

        for run in pRuns:
            runWidgetItem = QTreeWidgetItem(self.ui.resultsTreeWidget)
            #runItem.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsSelectable | Qt.ItemIsEnabled )
            runWidgetItem.setCheckState(0,Qt.Unchecked)
            runWidgetItem.setText (0,str( run.runInfo["LocalRunNumber"] ) )
            runWidgetItem.setText (1,str( run.runInfo["Date"] ) )
            runWidgetItem.setText (2,str( run.runInfo["Operator"] ) )
            runWidgetItem.setText (3,str( run.runInfo["Location"] ) )
            runWidgetItem.setText (4,str( run.runInfo["Result_Folder"] ) )
            runWidgetItem.setText (6,str( "" ) )

            for runItem in run.runItems:
                resultItem = QTreeWidgetItem()
                runWidgetItem.addChild( resultItem )
                resultItem.setText( 4, os.path.basename( runItem.summaryFile ) )
                for dataFile in runItem.dataFiles:
                    resultItem = QTreeWidgetItem()
                    runWidgetItem.addChild( resultItem )
                    resultItem.setText( 4, os.path.basename(dataFile["FileName"]) )
                    resultItem.setText( 5, str( dataFile["DBFile"]   ) )
                    resultItem.setText( 7, str( dataFile["Uploaded"] ) )


                #resultItem.setText( 6, str( result.comment      ) )
                #resultItem.setText( 7, str( result.dbRunNumber  ) )


        for column in range ( self.ui.resultsTreeWidget.columnCount()):
            self.ui.resultsTreeWidget.resizeColumnToContents( column )
        self.ui.resultsTreeWidget.expandToDepth(0)

        """
                if run.runInfo["LocalRunNumber"] == runIndex:
                    itemIndex = index

                    break
            #No Run item found


            resultItem.setText( 4, str( result.file         ) )
            resultItem.setText( 5, str( result.dbFile       ) )
            resultItem.setText( 6, str( result.comment      ) )
            resultItem.setText( 7, str( result.dbRunNumber  ) )
            if "convert" in result.status or "Failed" in result.status:
                resultItem.setForeground( 8 , Qt.red )
            elif "Converted" in result.status:
                resultItem.setForeground( 8 , Qt.darkYellow )
            elif "Uploaded" in result.status:
                resultItem.setForeground( 8 , Qt.green )
            resultItem.setText( 8, str( result.status       ) )
            
        aFileIsNotConverted = False
        for result in pResults:
            if "convert" in result.status:
                aFileIsNotConverted = True
                break
        if aFileIsNotConverted:
            self.ui.convertAllResultsButton.setStyleSheet("background-color: orange")
        else:
            self.ui.convertAllResultsButton.setStyleSheet("background-color: green")

        aFileIsNotUploaded = False
        for result in pResults:
            if "convert" in result.status or "Converted" in result.status:
                aFileIsNotUploaded = True
                break
        if aFileIsNotUploaded:
            self.ui.uploadAllResultsButton.setStyleSheet("background-color: orange")
        else:
            self.ui.uploadAllResultsButton.setStyleSheet("background-color: green")
        """
    #Let the gift in the corner rotate or not
    def setGiphtBusy( self, pBusy):
        if pBusy:
            self.movie = QMovie("./Utils/gipht_busy.gif")
            self.ui.GiphtIcon.setMovie(self.movie)
            self.ui.GiphtIcon_2.setMovie(self.movie)
            self.movie.start()
        else:
            self.ui.GiphtIcon.setPixmap(QPixmap("./Utils/gift.png"))
            self.ui.GiphtIcon.setScaledContents( True )
            self.ui.GiphtIcon.setSizePolicy(QSizePolicy.Ignored,QSizePolicy.Ignored)
            self.ui.GiphtIcon_2.setPixmap(QPixmap("./Utils/gift.png"))
            self.ui.GiphtIcon_2.setScaledContents( True )
            self.ui.GiphtIcon_2.setSizePolicy(QSizePolicy.Ignored,QSizePolicy.Ignored)

            # Optional, resize window to image size

