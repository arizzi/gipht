#------------------------------------------------
#+++++++++++++Arduino.py++++++++++++++++++++++
#Created by Stefan Maier            10.07.2020
#Last modified by Stefan Maier      17.08.2020
#------------------------------------------------
from datetime import datetime, timedelta
from src.Device import Device
import yaml
from PySide2.QtCore import QThread

class LED():
    def __init__( self, pIndex, pConfig):
        self.index = pIndex
        self.config = pConfig
        self.config["ID"] = "Top" + pConfig["Channel"] if int(pConfig["Channel"]) < 8 else "Bot" + str( int (pConfig["Channel"]) - 8 )

class Arduino(Device):
    possibleConnections = ["Serial"]
    def __init__( self, pIndex, pConfig, pDevicePackageFolder ):
        self.leds = []
        #These configs are unique for Arduino devices
        pConfig["Type"]     = "Arduino"
        pConfig["Model"]    = "ArduinoKIRA"
        pConfig["Connection"] = "Serial"
        if "Trigger" not in pConfig.keys():
            pConfig["Trigger"] = "on"
        if "TriggerFrequency" not in pConfig.keys():
            pConfig["TriggerFrequency"] = "1000"
        if "DacLed" not in pConfig.keys():
            pConfig["DacLed"] = "low"
        if "PulseLength" not in pConfig.keys():
            pConfig["PulseLength"] = "50"
        if "LEDs" not in pConfig.keys():
            pConfig["LEDs"] = self.writeDefaultLeds()
        if "ID" not in pConfig.keys():
            pConfig["ID"] = str(pIndex)+pConfig["Model"]

        self.modelDictionary = {"ArduinoKIRA": "ArduinoKIRA"}

        self.pollTimeout = 1000 #overwritten by GUI input

        self.monitorData = {"Timestamp":    [],
                            "temperature":  [],
                            "humidity":     [],
                            "dewpoint":     []}

        self.rh = "0"
        self.temp = "0"
        self.dp = "0"
        self.version = "<2.0"

        #Call Device constructtor
        super(Arduino, self).__init__( pIndex, pConfig , pDevicePackageFolder )

        self.updateXmlFile()
        self.createInterface( self.xmlFile )
        self.changeAndConnectWidget()

    def writeDefaultLeds( self ):
        ledList=[]
        for i in range(16):
            led = { "Channel":  str(i),
                    "Intensity":str(30000),
                    "Light": "off"} 
            ledList.append(led)
            self.leds.append(LED( len(self.leds),led))
        return ledList



    #Generates the LED objects based on a dictionary
    def updateLeds( self, pListOfLedConfigs ):
        self.leds.clear()
        for config in pListOfLedConfigs:
            self.leds.append (LED( len(self.leds), config) )

    def updateKiraSettings( self, pLedConfigs ):
        for item, value in pLedConfigs.items():
            self.config[item] = value
            if item == "LEDs":
                self.updateLeds(value)
        self.updateXmlFile()
        self.deviceCommand.emit(self.index,"Save", False, None)

    def getLedId( self, pChannel):
        return [led.config["ID"] for led in self.leds if led.config["Channel"] == str(pChannel)][0]

    def connectDataInterface( self ):
        try:
            self.data.disconnect()
        except RuntimeError:
            pass
        self.data.connect(self.manageArduinoData)
    
    
    #configureLedsAndTriggerButton
    
    #Data consists of the channelID, the variable and value
    def manageArduinoData(self, pData):
        #print( str(self.index) + "\t" + str(pData) )
        #channelId, what, data = pData.split('>')

        #Data of all Arduinos is comming through here, now select the fitting value
        #print(pData)
        if len(pData.split(',') )> 1:
            timestamp, data = self.decodeStatus( pData )
            timestamp = str((datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S") + timedelta(seconds=1) ).strftime("%Y-%m-%d %H:%M:%S"))
            self.lastReadout = datetime.now()#datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S")

            #print(data)

            if self.status != "ReadingData":
                self.status = "ReadingData"
                self.statusUpdate.emit(self)

        #print (data[self.config["ID"]] [self.config["ID"]] )
        
            if self.config["ID"] in data.keys():
                if 'Version' in data[self.config["ID"]][self.config["ID"]]:
                    self.version = str ( data[self.config["ID"]][self.config["ID"]]["Version"] )
                else:
                    self.rh    = data[self.config["ID"]][self.config["ID"]]["humidity"]
                    self.temp  = data[self.config["ID"]][self.config["ID"]]["temperature"]
                    self.dp    = data[self.config["ID"]][self.config["ID"]]["dewpoint"]

                #print(temp, humidity)
                ts = datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S")
                data[self.config["ID"]][self.config["ID"]]["Timestamp"] = str(ts)
                self.monitorData["Timestamp"].append( ts )
                self.monitorData["temperature"].append( float ( self.temp ) )
                self.monitorData["humidity"].append( float ( self.rh ) )
                self.monitorData["dewpoint"].append( float ( self.dp ) )

                log = timestamp + ": " + self.config["ID"] + ":   " + "Temperature: " + self.temp + "   Humidity: " + self.rh + "   Dewpoint: " + self.dp
                self.logUpdate.emit(log)
                #print("GIPHT:\tArduino data (" + self.config["ID"]+"): " + str ( data[self.config["ID"]] ) )
                if self.storeMonitorData is not None:
                    with open ( self.storeMonitorData, 'a') as f:
                        yaml.dump([data[self.config["ID"]][self.config["ID"]]],f)
        else:
            print("GIPHT:\tArduino response (" + self.config["ID"]+"): " + str(pData))




    #-----------------------------------------------------------------------
    #--------------------------------- Arduino poll ------------------------
    #-----------------------------------------------------------------------

    #Poll the arduino to see if there is a readout possible
    def check( self):
        self.serverOrPipeStatus =  self.hwInterface.server.isRunning()
        self.readoutArduino()
        self.getFWVersion()

    def monitor( self , pDumpToFile= None):
        self.readoutArduino()
        self.storeMonitorData = pDumpToFile

    def getFWVersion( self ):
        print( "GIPHT:\tArduino (" + self.config["ID"] + ") - Readout FW Version")
        cmd = "GetKIRAFWVersion,ArduinoId:" + self.config["ID"]
        self.task = "Poll FW of arduino"
        self.execute(cmd, True)

    #Gives the readout command to the Ph2_ACF
    def readoutArduino( self ):
        if self.hwInterface.running:
            print( "GIPHT:\tArduino (" + self.config["ID"] + ") - Readout")
            cmd = "GetArduinoStatus,ArduinoId:" + self.config["ID"]
            self.task = "Poll arduino"
            self.execute(cmd, True)

    def updateAndWriteKIRASettings( self, pSettingsDict ):
        self.updateKiraSettings( pSettingsDict )
        self.writeKiraSettings()

    def writeKiraSettings( self ):
        cmdList = []
        for key in ["Trigger","TriggerFrequency","DacLed","PulseLength"]:
            cmdList.append( "KIRA" + str(key) + ",ArduinoId:" + self.config["ID"] + ","+ str(key) +":" + str(self.config[key]) )

        for led in self.leds:
            cmdList.append("KIRALight" + ",ArduinoId:" + self.config["ID"] + ",LED:" + led.config["ID"] + ",Light:" + led.config["Light"]  )
            cmdList.append("KIRAIntensity" + ",ArduinoId:" + self.config["ID"] + ",LED:" + led.config["ID"] + ",Intensity:" + led.config["Intensity"]  )            

        self.task = "Adjust KIRA settings"
        #[print(cmd) for cmd in cmdList]

        for cmd in cmdList:
            self.execute(cmd, True)
            QThread.msleep(20)

    def turnOffLeds( self ):
        cmdList = []
        for led in self.leds:
            cmdList.append("KIRALight" + ",ArduinoId:" + self.config["ID"] + ",LED:" + led.config["ID"] + ",Light:off")
        self.task = "Adjust KIRA settings"
        for cmd in cmdList:
            self.execute(cmd, True)


#KIRAPulseLength,ArduinoId:abc,PulseLength:123
#KIRALight,ArduinoId:abc,LED:led1,Light:on/off
#KIRAIntensity,ArduinoId:abc,LED:led1,Intensity:123
#IRAIntensity,ArduinoId:abc,LED:led1,Intensity:123
#KIRAPulseLength,ArduinoId:abc,PulseLength:123
#KIRAPulseLength,ArduinoId:abc,PulseLength:123

#[{'Channel': '0', 'Intensity': '30000', 'Light': 'on'},