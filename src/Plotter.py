import matplotlib

import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

from matplotlib.widgets import CheckButtons
import yaml, os
from datetime import datetime


class MonitorPlot():
    def __init__( self, pFileOrData, pMode, pPathToSaveFigure = None ):

        self.outputFile = pPathToSaveFigure
        if isinstance(pFileOrData, dict):
            self.fileContent = pFileOrData
        else:
            if os.path.isfile(pFileOrData):
                with open(pFileOrData, 'r') as f:
                    self.fileContent = yaml.safe_load(f)

        #if self.outputFile is not None:
        #    matplotlib.use('agg')
        #else:
        #    matplotlib.rcParams['backend'] = 'QtAgg' 
        self.mode = pMode

        if self.mode in ["LV","HV"]:
            self.subMode =      ["Voltage","Current","TimeStamps"]
            self.clickLabels =  ["Voltage","Current","TimeStamps"]
        elif self.mode == "Environment":
            self.subMode =      ["Humidity","Temperature","TimeStamps"]
            self.clickLabels =  ["Humidity","Temperature","TimeStamps"]
        self.fig, self.axL = plt.subplots(figsize=(16, 9))
        self.axR = None
        self.rax = None
        self.generatePlot()

    def generatePlot( self ):
        self.axL.set_xlabel("Time")
        #add 1 second because of differen time resolution of powersupply return data and task object + timedelta(seconds=1)
        timestamps = [ datetime.strptime(stime, "%Y-%m-%d %H:%M:%S") for stime in self.fileContent["Data"][self.mode]["Timestamps"] ]
        customLabels = []
        customLegend = []
        if self.mode == "LV":
            self.fig.canvas.manager.set_window_title('Low Voltage')
            if "Voltage" in self.subMode:
                self.axL.spines['left'].set_color('b')
                self.axL.tick_params(axis='y', colors = 'b')
                self.axL.set_ylabel('Voltage (V)')
                self.axL.yaxis.label.set_color('b')
                self.axL.plot(timestamps, [float(x) for x in self.fileContent["Data"]["LV"]["Voltages"] ], label= "LV Voltage", color = "b" )
                customLegend.append(Line2D([0],[0], color = 'b'))
                customLabels.append("LV Voltage")
                if "Current"  in self.subMode:
                    self.axR = self.axL.twinx()
                    self.axR.yaxis.label.set_color('r')
                    self.axR.set_ylabel('Current (A)')
                    self.axR.spines['right'].set_color('r')
                    self.axR.tick_params(axis='y', colors = 'r')
                    xon, yon, xoff, yoff, yonoff = self.getCurrentsDependingOnOutput(timestamps, self.fileContent["Data"]["LV"])
                    self.axR.plot(timestamps, [float(y) for y in yonoff ], label= "LV Current sum" ,color = "r" )
                    self.axR.scatter(xon, [float(y) for y in yon ], label= "LV Current" ,color = "r" )
                    self.axR.scatter(xoff, [float(y) for y in yoff ], label= "LV Current Ch OFF" ,color = "darkred" )
                    customLegend.append(Line2D([0],[0], color = 'r'))
                    customLabels.append("LV Current")
            if "Current" in self.subMode and "Voltage" not in self.subMode:
                self.axL.spines['left'].set_color('r')
                self.axL.tick_params(axis='y', colors = 'r')
                self.axL.set_ylabel('Current (A)')
                self.axL.yaxis.label.set_color('r')
                xon, yon, xoff, yoff, yonoff = self.getCurrentsDependingOnOutput(timestamps, self.fileContent["Data"]["LV"])
                self.axL.plot(timestamps, [float(y) for y in yonoff ], label= "LV Current sum" ,color = "r" )
                self.axL.scatter(xon, [float(y) for y in yon ], label= "LV Current" ,color = "r" )
                self.axL.scatter(xoff, [float(y) for y in yoff ], label= "LV Current Ch OFF" ,color = "darkred" )
                customLegend.append(Line2D([0],[0], color = 'r'))
                customLabels.append("LV Current")

            if self.outputFile is None:
                self.rax = self.fig.add_axes([0.025, 0.75, 0.08, 0.12])
                self.check = CheckButtons(
                    ax=self.rax,
                    labels=self.clickLabels,
                    actives= [entry in self.subMode for entry in self.clickLabels],
                    label_props={'color': ["b","r","k"]},
                    frame_props={'edgecolor': ["b","r","k"]},
                    check_props={'facecolor': ["b","r","k"]},
                )
                self.check.on_clicked(self.callbackLVHV)
        elif self.mode == "HV":
            self.fig.canvas.manager.set_window_title('High Voltage')
            if "Voltage" in self.subMode:
                self.axL.spines['left'].set_color('b')
                self.axL.tick_params(axis='y', colors = 'b')
                self.axL.set_ylabel('Voltage (V)')
                self.axL.yaxis.label.set_color('b')
                self.axL.plot(timestamps, [float(x) for x in self.fileContent["Data"]["HV"]["Voltages"] ], label= "HV Voltage", color = "b" )
                customLegend.append(Line2D([0],[0], color = 'b'))
                customLabels.append("HV Voltage")
                if "Current"  in self.subMode:
                    self.axR = self.axL.twinx()
                    self.axR.yaxis.label.set_color('r')
                    self.axR.set_ylabel('Current (A)')
                    self.axR.spines['right'].set_color('r')
                    self.axR.tick_params(axis='y', colors = 'r')
                    xon, yon, xoff, yoff, yonoff = self.getCurrentsDependingOnOutput(timestamps, self.fileContent["Data"]["HV"])
                    self.axR.plot(timestamps, [float(y) for y in yonoff ], label= "HV Current sum" ,color = "r" )
                    self.axR.scatter(xon, [float(y) for y in yon ], label= "HV Current" ,color = "r" )
                    self.axR.scatter(xoff, [float(y) for y in yoff ], label= "HV Current Ch OFF" ,color = "darkred" )
                    customLegend.append(Line2D([0],[0], color = 'r'))
                    customLabels.append("HV Current")
            if "Current" in self.subMode and "Voltage" not in self.subMode:
                self.axL.spines['left'].set_color('r')
                self.axL.tick_params(axis='y', colors = 'r')
                self.axL.set_ylabel('Current (A)')
                self.axL.yaxis.label.set_color('r')
                xon, yon, xoff, yoff, yonoff = self.getCurrentsDependingOnOutput(timestamps, self.fileContent["Data"]["HV"])
                self.axL.plot(timestamps, [float(y) for y in yonoff ], label= "HV Current sum" ,color = "r" )
                self.axL.scatter(xon, [float(y) for y in yon ], label= "HV Current" ,color = "r" )
                self.axL.scatter(xoff, [float(y) for y in yoff ], label= "HV Current Ch OFF" ,color = "darkred" )
                customLegend.append(Line2D([0],[0], color = 'r'))
                customLabels.append("HV Current")

            if self.outputFile is None:
                self.rax = self.fig.add_axes([0.025, 0.75, 0.08, 0.12])
                self.check = CheckButtons(
                    ax=self.rax,
                    labels=self.clickLabels,
                    actives= [entry in self.subMode for entry in self.clickLabels],
                    label_props={'color': ["b","r","k"]},
                    frame_props={'edgecolor': ["b","r","k"]},
                    check_props={'facecolor': ["b","r","k"]},
                )
                self.check.on_clicked(self.callbackLVHV)
        elif self.mode == "Environment":
            self.fig.canvas.manager.set_window_title('Humidity and Temperature')
            if "Humidity" in self.subMode:
                self.axL.spines['left'].set_color('b')
                self.axL.tick_params(axis='y', colors = 'b')
                self.axL.set_ylabel('Humidity (%)')
                self.axL.yaxis.label.set_color('b')
                self.axL.plot(timestamps, [float(x) for x in self.fileContent["Data"]["Environment"]["Humidities"] ], label= "Humidity", color = "b" )
                customLegend.append(Line2D([0],[0], color = 'b'))
                customLabels.append("Humidity")
                if "Temperature"  in self.subMode:
                    self.axR = self.axL.twinx()
                    self.axR.yaxis.label.set_color('r')
                    self.axR.set_ylabel('Temperature (°C)')
                    self.axR.spines['right'].set_color('r')
                    self.axR.tick_params(axis='y', colors = 'r')
                    self.axR.plot(timestamps, [float(x) for x in self.fileContent["Data"]["Environment"]["Temperatures"] ], label= "Temperature" ,color = "r" )
                    customLegend.append(Line2D([0],[0], color = 'r'))
                    customLabels.append("Temperature")
            if "Temperature" in self.subMode and "Humidity" not in self.subMode:
                self.axL.spines['left'].set_color('r')
                self.axL.tick_params(axis='y', colors = 'r')
                self.axL.set_ylabel('Temperature (°C)')
                self.axL.yaxis.label.set_color('r')
                self.axL.plot(timestamps, [float(x) for x in self.fileContent["Data"]["Environment"]["Temperatures"] ], label= "Temperature" ,color = "r" )
                customLegend.append(Line2D([0],[0], color = 'r'))
                customLabels.append("Temperature")

            if self.outputFile is None:
                self.rax = self.fig.add_axes([0.025, 0.75, 0.08, 0.12])
                self.check = CheckButtons(
                    ax=self.rax,
                    labels=self.clickLabels,
                    actives= [entry in self.subMode for entry in self.clickLabels],
                    label_props={'color': ["b","r","k"]},
                    frame_props={'edgecolor': ["b","r","k"]},
                    check_props={'facecolor': ["b","r","k"]},
                )
                self.check.on_clicked(self.callbackRHT)


        if "TimeStamps" in self.subMode and self.outputFile is None:
            self.drawTasks( )

        self.axL.legend(customLegend, customLabels)
        self.axL.grid()

        if self.outputFile is None:
            self.fig.show()
            self.fig.canvas.draw()
            self.fig.canvas.flush_events()

        else:
            self.fig.savefig(self.outputFile)
            plt.close(self.fig)

    def callbackLVHV(self, label):
        tempCopy = self.subMode.copy()
        if label in self.subMode:
            tempCopy.remove(label)
        else:
            tempCopy.append(label)

        if "Voltage" not in tempCopy and "Current" not in tempCopy:
            self.check.eventson = False
            self.check.set_active(self.clickLabels.index(label))
            self.check.eventson = True
        else:
            self.subMode = tempCopy.copy()
        self.axL.cla()
        if self.axR is not None:
            self.axR.cla()
            self.axR.yaxis.set_tick_params(labelbottom=False)
            self.axR.yaxis.set_tick_params(labelleft=False)
            self.axR.set_yticks([])
        self.generatePlot()

    def callbackRHT(self, label):
        tempCopy = self.subMode.copy()
        if label in self.subMode:
            tempCopy.remove(label)
        else:
            tempCopy.append(label)

        if "Temperature" not in tempCopy and "Humidity" not in tempCopy:
            self.check.eventson = False
            self.check.set_active(self.clickLabels.index(label))
            self.check.eventson = True
        else:
            self.subMode = tempCopy.copy()
        self.axL.cla()
        if self.axR is not None:
            self.axR.cla()
            self.axR.yaxis.set_tick_params(labelbottom=False)
            self.axR.yaxis.set_tick_params(labelleft=False)
            self.axR.set_yticks([])

        self.generatePlot()

    def drawTasks( self ):
        for task in self.fileContent["Tasks"]:
            if not task["Task"] in ["MonitorPoll", "AnalyseMonitorData"]:
                ts = datetime.strptime(task["Timestamp"], "%Y-%m-%d %H:%M:%S.%f")
                self.axL.axvline( ts , color = 'k')
                self.axL.text(ts, self.axL.get_ylim()[1] + (self.axL.get_ylim()[0]-self.axL.get_ylim()[1])/2, task["Information"], rotation=90, verticalalignment='center', color = 'k',clip_on=True)

    def getCurrentsDependingOnOutput( self, pTimestamps, pData):
        xon, yon, xoff, yoff, yonoff = [[] for i in range(5)]
        for i, on in enumerate(pData["Ons"]):
            if on == '1':
                xon.append(pTimestamps[i])
                yon.append(pData["Currents"][i])
                yonoff.append(pData["Currents"][i])

            elif on == '0':
                xoff.append(pTimestamps[i])
                yoff.append(0)
                yonoff.append(0)

        return xon, yon, xoff, yoff, yonoff

class SummaryText():
    def __init__( self, pSummary, pFile ):
        self.sum, self.ax = plt.subplots(figsize=(7, 4))
        i = 0
        for key, value in pSummary.items():
            self.ax.text(0.05,0.07*i, str(key) + ": " + str(value))
            i +=1
        self.ax.axis("off")
        self.sum.savefig(pFile)
        plt.close(self.sum)
        #self.sum.show()


class IvPlot():
    def __init__( self, pSlotOrFile, pFromFile = False ):
        print("Backend used by matplotlib is: ", matplotlib.get_backend())
        self.ivFig = plt.gcf()
        self.ivFig.canvas.draw()
        if pFromFile:
            #Read file
            u = []
            i = []
            with open(pSlotOrFile, 'r') as f:
                fileContent = yaml.safe_load(f)
                for point in fileContent["Data"]:
                    u.append( float( point["Voltage"] ) )
                    i.append( float( point["Current"] ) )
            self.ivFig.canvas.manager.set_window_title("I(V) Measurement module " + fileContent["Info"]["ID"] )
            self.update( u, i )
        else:
            self.ivFig.canvas.manager.set_window_title("I(V) Measurement module " + pSlotOrFile.module.id)

    def update( self , pV_meas, pI_meas):

        u = []
        i = []
        plt.cla()
        plt.xlabel('Voltage (V)')
        plt.ylabel('Current (nA)')
        plt.title("I(V) Measurement")
        plt.grid(True)
        for index in range (len ( pI_meas) ) :
            u.append( pV_meas[index] )
            i.append( pI_meas[index] )

        self.ivFig.show()
        
        plt.plot(u, i, lw=2, color='red')
        self.ivFig.canvas.draw()
        self.ivFig.canvas.flush_events()


    def save( self, pFile ):
        plt.savefig(pFile)

    def close ( self ):
        self.ivFig.clf()