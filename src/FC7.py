#------------------------------------------------
#+++++++++++++FC7Board.py++++++++++++++++++++++
#Created by Stefan Maier            10.07.2020
#Last modified by Stefan Maier      26.11.2020
#------------------------------------------------

import subprocess, re, time
from src.Device import Device

CGREEN = '\033[32m'    # For green prints
CRED   = '\033[91m'   # For red prints
CEND   = '\033[0m'

dummyTest = False

class FC7(Device):

    def __init__(self, pIndex, pConfig, pPh2AcfFolder, pAddressTable):
        pConfig["Type"]     = "FC7"
        pConfig["Model"]    = "FC7"
        pConfig["Connection"] = "Ethernet"
        pConfig["Address_Table"] = pAddressTable
        if "ID" not in pConfig.keys():
            pConfig["ID"] = str(pIndex)+pConfig["Model"]
        self.modelDictionary = {"FC7": "FC7"}

        super(FC7, self).__init__( pIndex, pConfig, pPh2AcfFolder )

        print("GIPHT:\tNew FC7 board (index: " + str(pIndex) + ")")

        self.nHybrids       = 0
        self.nChips         = 0
        self.chipType       = ""
        self.timeStamp      = ""
        self.firmwareName   = "-"
        self.firmwareList   = []
 
        self.availableFc7s  = []
 
        self.updateXmlFile()
        self.createInterface( self.xmlFile )
        self.changeAndConnectWidget()



    #-----------------------------------------------------------------------
    #------ Get information from the board and handle the data stream  -----
    #-----------------------------------------------------------------------
    def connectDataInterface( self ):
        self.data.connect(self.manageFc7Data)

    #Manages information comming from the FC7 about the loaded firmware
    def manageFc7Data(self, pData):
        
        print("GIPHT:\tFC7 Board data (" + self.config["ID"] + ", " + self.config["IPAddress"] + "): " + str(pData) )
        what, data = pData.split('>')
        if "Firmware" in what:
            self.firmwareList.append(data)
            self.firmwareList = sorted ( list ( set ( self.firmwareList ) ) ) #Make the list entries unique
        elif "timeStamp" in what:
            self.timeStamp = data
        elif "nHybrids" in what:
            self.nHybrids = data
        elif "nChips" in what:
            self.nChips = data
        elif "chipType" in what:
            self.chipType = data
        if "NewFirmware" in what:
            self.firmwareName = data
        log = "FC7(" + self.config["ID"] + ", " + self.config["IPAddress"] + "): " + what +":\t" + data
        self.logUpdate.emit( log )



    #---------------------------------------------------------------------
    #Search for FC7 in the network (Maybe moved to the Controller or Utils
    #---------------------------------------------------------------------



     #started by the button in the fc7 tab, rescans the network for fc7s
    def findFC7sInNetwork( self ):
        #Ping complete network
        self.pingNetwork()
        #Check result of the arp -n command
        arpCache  = self.getArpCache()
        filteredList = self.filterForFc7Macs(arpCache)
        ipList = {}
        #Check if the fc7s are r
        for ip, mac in filteredList.items():
            if self.pingIp(ip) == 0:
                ipList[ip] = mac

        ips = []
        for ip, mac in ipList.items():
            ips.append(ip)
        self.availableFc7s = ips
        return ips

    #First checks the ip address(es) of the local machine. Then all Ips 0 to 255 are pinged
    #This is done that the arp cache is updated
    def pingNetwork( self ):
        print("GIPHT:\tPing network")
        ipList = {}

        #Get Ip addresses of host
        cmd = "hostname -I"
        ipAddressHostString = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        ipAddressHostListString = str(ipAddressHostString.communicate())
        ipAddressHostList = re.findall(r'[0-9]+(?:\.[0-9]+){3}', ipAddressHostListString )

        #Send ping routine for each possible connection
        for ip in ipAddressHostList:
            #Extract ip address space XXX.XXX.XXX.___ Remove last digit from ip address
            while True:
                if (ip[-1] != '.' ):
                    ip = ip[:-1]
                else:
                    break
            #Ping all possibilities in address space
            cmd = "seq 254 | xargs -iIP -P255 ping -c1 " + ip + "IP |grep time="
            pinging = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    #Calls arp -n and returns the list of ip and macs in the network
    def getArpCache ( self ):
        p = subprocess.Popen("arp -n", shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        #Check result of the arp -n command
        ret = {}
        for line in p.stdout.readlines():
            entries = line.split()
            currentIP = str(entries[0]).replace('b','').replace('\'','')
            currentMAC = str(entries[2]).replace('b','').replace('\'','')
            ret[currentIP] = currentMAC
        return ret

    #An FC7 has a rather unique start of the MAC address, filter for it and return combinations that fit the subMAC
    def filterForFc7Macs( self, pIpCacheDictionary ):
        ret = {}
        for ip, mac in pIpCacheDictionary.items():
            subMACAddress = "08:00:30:00:"
            if subMACAddress in mac:
                print("GIPHT:\t" + CGREEN + "Found FC7 MAC in IP List:\tIP: " + ip + "\tMAC: " + mac + CEND)
                ret[ip] = mac
        return ret

    #Ping a single ip with and wait for one pacakge, return the package loss. Etiher 0 or 100 %
    #Takes quite a while before if there were ips in the arp cache but not are not reachable anymore
    def pingIp( self, pIp ):
        singlePingCmd = "ping -c 1 " + pIp
        print("GIPHT:\tPing Board: " + singlePingCmd)

        singlePing = subprocess.Popen(singlePingCmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        out, err = singlePing.communicate()

        packetLoss = float(str(out).split('%')[0].split(',')[-1].replace(' ',''))

        if packetLoss == 0:
            print("GIPHT:\t" + CGREEN + "Ping succesful: " +str( packetLoss ) + "% loss" + CEND)
        else:
            print("GIPHT:\t" + CRED + "Ping unsuccesful: " + str( packetLoss ) + "% loss" + CEND)
        return packetLoss


    #-----------------------------------------------------------------------
    #---------------- Start settings and info tasks on the board -----------
    #-----------------------------------------------------------------------

    #If you can retrieve FW information, the FC7 is ready to use
    def check( self ):
        #Check pipe here?
        self.getFirmwareInformation()

    def monitor( self ):
        self.getFirmwareInformation()


    #Actually calls the command to get the firmware info
    def getFirmwareInformation(self):
        self.firmwareList.clear()
        print( "GIPHT:\tGet firmware information of board " + self.config.get("IPAddress","") )
        self.task = "Get firmware information"
        self.status = "Busy"
        self.statusUpdate.emit(self)
        time.sleep(0.01)
        self.hwInterface.listFirmware(self.xmlFile, 0)


    #switch firmware to another firmware loaded
    def switchFirmware( self, pFirmware ):
        print( "GIPHT:\tSwitch firmware on board " + self.config["IPAddress"] + " to " + pFirmware)
        self.task = "Switch firmware"
        self.status = "Busy"
        self.statusUpdate.emit(self)
        time.sleep(0.01)
        self.hwInterface.loadFirmware(self.xmlFile, pFirmware, 0)

    #switch firmware to another firmware loaded
    def uploadFirmware( self, pFirmware ):
        print("GIPHT:\tUpload firmware "+ pFirmware +" to board " + self.config["IPAddress"])
        self.task = "Upload firmware"
        self.status = "Busy"
        self.statusUpdate.emit(self)
        time.sleep(0.01)
        self.hwInterface.uploadFirmware(self.xmlFile, pFirmware, 0)

    def turnOffVTRXLight( self, pXmlFile ):
        print( "GIPHT:\tTurn light OFF" )
        self.task = "Running: Light Off"

        self.status = "Busy"
        self.statusUpdate.emit(self)
        self.hwInterface.turnOffVTRXLight(pXmlFile)

    def moduleTest( self, pXmlFile, pResultDirectory, pRunNumber, pSelectedMeasurements ):

        infostring = ""
        for entry in pSelectedMeasurements:
            if entry not in ["IV", "FullTest"]:
                infostring = infostring + " " + entry +","
        print( "GIPHT:\tStart " + infostring + " measurement" )

        self.task = "Running: Measure" + infostring

        self.status = "Busy"
        self.statusUpdate.emit(self)
        self.hwInterface.moduleTest(pXmlFile, 0, pResultDirectory, pRunNumber, pSelectedMeasurements)
    
    def dataTest( self, pXmlFile, pResultDirectory, pRunNumber ):
        self.task = "Take data"

        self.status = "Busy"
        self.statusUpdate.emit(self)
        self.hwInterface.dataTest(pXmlFile, 0, pResultDirectory, pRunNumber)
    