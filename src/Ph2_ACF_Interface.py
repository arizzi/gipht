from PySide2.QtCore import Signal, QObject
import sys, os

class Ph2_ACF_Interface(QObject):

    update = Signal ( object , object )

    def __init__( self, pPh2AcfFolder, pType, pIndex ):
        super(Ph2_ACF_Interface, self).__init__(  )
        self.ph2AcfFolder   = pPh2AcfFolder
        self.index = pIndex
        self.importPh2ACF()

    def importPh2ACF( self ):
        try:
            sys.path.insert(1, os.getenv('PH2ACF_BASE_DIR'))
            import pythonUtils.Ph2_ACF_StateMachine as Ph2_ACF_StateMachine
            import lib.Ph2_ACF_PythonInterface as Ph2_ACF
            Ph2_ACF.configureLogger(os.getenv('PH2ACF_BASE_DIR') + "/settings/logger.conf")
            self.sm = Ph2_ACF_StateMachine.StateMachine()
            return True
        except ModuleNotFoundError:
            print("Ph2_ACF not reachable: ModuleNotFoundError. Check python version for compilation of pybind11 package")
            return False

    def listFirmware( self, pConfig, pBoard):
        try:
            if self.importPh2ACF():
                firmwareList = self.sm.listFirmware(pConfig,pBoard)
                for firmware in firmwareList:
                    self.update.emit("data","Firmware>"+firmware)
        except:
            print("FW list empty")



    def loadFirmware(self, pConfig, pName, pBoard):
        if self.importPh2ACF():
            self.sm.loadFirmware(pConfig,pName,pBoard)
            if self.sm.isSuccess():
                self.update.emit("data", "NewFirmware>"+pName)

    def uploadFirmware(self, pConfig, pFirmware, pBoard):
        firmwarePath = "./Bitstreams/" + pFirmware
        if self.importPh2ACF():
            self.sm.uploadFirmware(pConfig, pFirmware, firmwarePath, pBoard)
            if self.sm.isSuccess():
                self.update.emit("data", "NewFirmware>"+pFirmware)

    def turnOffVTRXLight(self, pConfig):
        #Resultsdirectory must be added in python ph2acf
        if self.importPh2ACF():
            calibrationname = "vtrxoff"

            self.sm.setCalibrationName(calibrationname)
            self.sm.setConfigurationFiles(pConfig)

            self.sm.runCalibration()
            self.update.emit("finished",None)
        else:
            return

    def moduleTest(self, pConfig, pBoard, pResultDirectory ,pRunNumber, pSelectedMeasurements):
        #Resultsdirectory must be added in python ph2acf
        if self.importPh2ACF():

            calibrationname = "calibration"
            if "Noise" in pSelectedMeasurements:
                calibrationname += "andpedenoise"
            if "KIRA" in pSelectedMeasurements:
                calibrationname += "andkira"

            self.sm.setCalibrationName(calibrationname)
            self.sm.setConfigurationFiles(pConfig)

            os.environ['GIPHT_RESULT_FOLDER'] = pResultDirectory
            self.sm.setRunNumber(pRunNumber)
            self.sm.runCalibration()
            self.update.emit("finished",None)
        else:
            return

