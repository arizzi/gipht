#------------------------------------------------
#+++++++++++++XMLManipulator.py++++++++++++++++++++++
#Created by Stefan Maier            10.07.2020
#Last modified by Stefan Maier      13.10.2022
#------------------------------------------------
import lxml.etree as ET
import re, os
from copy import deepcopy


class XMLManipulator():

    def __init__(self ):
        pass

    #-----------------------------------------------------------------------
    #-------- Manipulate hardware description file according to input  -----
    #-----------------------------------------------------------------------

    def copyXmlFile( self, pDestination, pSource):
        content = []
        success = True
        try:
            with open(pSource, 'r') as file:
                for line in file:
                    content.append(line)
        except FileNotFoundError:
            print("GIPHT:\tFile " + pSource + " does not exist!")
            success = False
        try:
            with open(pDestination, 'w') as file:
                for line in content:
                    file.write(line)
        except FileNotFoundError:
            print("GIPHT:\tFile " + pDestination + " does not exist!")
            success = False
        return success

    def exchangeIpAddress( self, pFile, pIpAddress):
        #Create HW description file from standard HW file with correct IP address
        try:    #Could be that the file is not available
            self.tree = ET.parse(pFile)
            self.root = self.tree.getroot()
            #connection_str = None
            #print(ET.tostring(self.root, pretty_print=True).decode())

            #Must loop into the nodes, although they exist only once
            for beBoard in self.root.iter("BeBoard"):
                for connection in beBoard.iter("connection"):

                    #print(ET.tostring(connection, pretty_print=True).decode())
                    connection_str = connection.get("uri")
                    #print(connection_str)
                    #print(pFile)
                    #print(pIpAddress)

                    self.connection_param = re.split(":",connection_str)
                    host = self.connection_param[1][2:]
                    self.target = re.split("=", self.connection_param[2])
                    connection_str = f"{self.connection_param[0]}://{host}:{self.target[0]}={pIpAddress}:{self.connection_param[3]}"
                    connection.set("uri",connection_str)
                    self.tree.write(pFile)
        except (OSError, FileNotFoundError,ET.XMLSyntaxError):
            print("GIPHT:\tCould not load HW File " + pFile )

    def editModuleBlock( self, pFile,pModules):
        self.tree = ET.parse(pFile)
        self.root = self.tree.getroot()

        #Extract the module block from the default file. Its the first Opictal group block
        otModuleBlock = None
        #Extract and then remove first optical block
        for beBoard in self.root.iter("BeBoard"):
            for opticalGroup in beBoard.iter("OpticalGroup"):
                otModuleBlock = opticalGroup
                opticalGroup.getparent().remove(opticalGroup)
                break

        for beBoard in self.root.iter("BeBoard"):
            for idIndex, module in reversed(list(enumerate(pModules))):

                otModuleBlock.set("Id",str(idIndex))
                for hybridIndex, hybrid in enumerate(otModuleBlock.iter("Hybrid")):
                    hybrid.set("Id",str(hybridIndex))
                beBoard.insert(3,deepcopy(otModuleBlock))

            #print(ET.tostring(otModuleBlock, pretty_print=True, xml_declaration=False).decode() )
            #print(ET.tostring(self.root, pretty_print=True, xml_declaration=False).decode() )
        #except:
        #    print("no file")
        

        """
        resultFolder = os.getenv('GIPHT_RESULT_FOLDER')
        if resultFolder:
            fileList = os.listdir(resultFolder) 
            print(fileList)
            if "BE0_OG0_FE0.txt" in fileList:
                for beIndex, beBoard in enumerate(self.root.iter("BeBoard")):
                    for ogIndex, opticalGroup in enumerate(beBoard.iter("OpticalGroup")):
                        for hybridIndex, hybrid in enumerate(opticalGroup.iter("Hybrid")):
                            for cbcFile in hybrid.iter("CBC_Files"):
                                cbcFile.set("path", resultFolder)
                            for cbcIndex, cbc in enumerate( hybrid.iter("CBC") ):
                                cbc.set("configfile", "BE" + str(beIndex) + "_OG" +str(ogIndex) + "_FE" + str(hybridIndex) + "_Chip" + str(cbcIndex) + ".txt" )
        """

        # Update the offset block


        stringToFile=ET.tostring(self.tree, pretty_print=True)
        with open(pFile, 'wb') as file:
            file.write(stringToFile)    

    def updateModuleTestSettings( self, pFile, pConfigList):
        self.tree = ET.parse(pFile)
        self.root = self.tree.getroot()
        for beBoard in self.root.iter("BeBoard"):
            for opticalGroup in beBoard.iter("OpticalGroup"):
                for hybrid in opticalGroup.iter("Hybrid"):
                    for settings in hybrid.iter("Settings"):
                        settings.set("latency", str (pConfigList["Latency"]))
                        settings.set("threshold", str (pConfigList["Threshold"]))



        for beBoard in self.root.iter("BeBoard"):
            for register in beBoard.iter("Register"):
                if register.get("name") =="trigger_source":
                    if pConfigList["External_Trigger"]:
                        register.text = "5"
                    else:
                        register.text = "3"

        for settings in self.root.iter("Settings"):
            for setting in settings.iter("Setting"):
                if setting.get("name") == "NeventsBeamTestCheck":
                    setting.text = pConfigList["N_Events"]

        stringToFile=ET.tostring(self.tree, pretty_print=True)


        with open(pFile, 'wb') as file:
            file.write(stringToFile)    

    def insertKIRAInformation( self, pFile, pPort, pId):
        self.tree = ET.parse(pFile)
        self.root = self.tree.getroot()

        for settings in self.root.iter("Settings"):
            for setting in settings.iter("Setting"):
                if setting.get("name") == "KIRA_Port":
                    setting.text = str(pPort)
                if setting.get("name") == "KIRA_ID":
                    setting.text = pId

        stringToFile=ET.tostring(self.tree, pretty_print=True)


        with open(pFile, 'wb') as file:
            file.write(stringToFile)            

    def editCicVersion(self, pFile, pCicVersion):
        self.tree = ET.parse(pFile)
        self.root = self.tree.getroot()
        for beBoard in self.root.iter("BeBoard"):
            for opticalGroup in beBoard.iter("OpticalGroup"):
                for hybrid in opticalGroup.iter("Hybrid"):
                    for globalNode in hybrid.iter("Global"):                        
                        for cic in globalNode.iter("CIC"):
                            #cic.tag = "CIC" if pCicVersion == 1 else "CIC2"
                            newCIC = ET.XMLParser().makeelement("CIC" if str(pCicVersion) == "1" else "CIC2", cic.attrib)
                            cic.getparent().remove(cic)
                            globalNode.insert(5,deepcopy(newCIC))
                        for cic in globalNode.iter("CIC2"):
                            #cic.tag = "CIC" if pCicVersion == 1 else "CIC2"
                            newCIC = ET.XMLParser().makeelement("CIC" if str(pCicVersion) == "1" else "CIC2", cic.attrib)
                            cic.getparent().remove(cic)
                            globalNode.insert(5,deepcopy(newCIC))
                    for cic in hybrid.iter("CIC"):
                        if "configfile" in cic.attrib:
                            newCIC = ET.XMLParser().makeelement("CIC" if str(pCicVersion) == "1" else "CIC2", cic.attrib)
                            cic.getparent().remove(cic)
                            newCIC.set("configfile", "CIC_default.txt" if str(pCicVersion) == "1" else "CIC2_default.txt")
                            hybrid.insert(11,deepcopy(newCIC))
                    for cic in hybrid.iter("CIC2"):
                        if "configfile" in cic.attrib:
                            newCIC = ET.XMLParser().makeelement("CIC" if str(pCicVersion) == "1" else "CIC2", cic.attrib)
                            cic.getparent().remove(cic)
                            newCIC.set("configfile", "CIC_default.txt" if str(pCicVersion) == "1" else "CIC2_default.txt")
                            hybrid.insert(11,deepcopy(newCIC))


        stringToFile=ET.tostring(self.tree, pretty_print=True)

        with open(pFile, 'wb') as file:
            file.write(stringToFile)            
    
    def editLpGBTVersion(self, pFile, pLpGBTVersion):
        self.tree = ET.parse(pFile)
        self.root = self.tree.getroot()
        for beBoard in self.root.iter("BeBoard"):
            for opticalGroup in beBoard.iter("OpticalGroup"):
                for lpGBT in opticalGroup.iter("lpGBT"):
                    lpGBT.set("version","0" if str(pLpGBTVersion) == "0" else "1")
                    lpGBT.set("configfile","lpGBT_v0.txt" if str(pLpGBTVersion) == "0" else "lpGBT_v1.txt")
                    
        stringToFile=ET.tostring(self.tree, pretty_print=True)

        with open(pFile, 'wb') as file:
            file.write(stringToFile)            
    

    #-----------------------------------------------------------------------
    #-------------------------   Manipulate Device xmls   ------------------
    #-----------------------------------------------------------------------
    def updateOrCreateDeviceFile ( self, pDevice , pOutputFile):
        if pDevice.config["Type"] == "PowerSupply":
            self.createNewPowerSupplyFile ( pDevice, pOutputFile )
        elif pDevice.config["Type"] == "Arduino":
            self.createNewArduinoFile ( pDevice, pOutputFile )
        elif pDevice.config["Type"] == "FC7":
            self.createFc7File( pDevice, pOutputFile )
        else:
            print("GIPHT:\t Unknown device type, pass...")

    def createFc7File( self, pFc7Board, pOutputFile):
        hwDescription = ET.Element("HwDescription")
        beBoard = ET.Element("BeBoard", Id="0", boardType= "D19C", eventType="VR")
        connection = ET.Element("connection",
                                                id="board",
                                                uri= "chtcp-2.0://localhost:10203?target="+ pFc7Board.config.get("IPAddress","") + ":50001",
                                                address_table="file://${PH2ACF_BASE_DIR}/" + pFc7Board.config.get("Address_Table","").replace(pFc7Board.folder,"") )

        beBoard.append(connection)
        hwDescription.append(beBoard)
        #print(ET.tostring(hwDescription, pretty_print=True).decode())
        str=ET.tostring(hwDescription, pretty_print=True)
        with open(pOutputFile, 'w+b') as file:
            file.write(str)

    def createFc7Entry( self, pFc7Board ):
        element = ET.Element("FC7",
                                            ID            = pFc7Board.config.get("ID",""),
                                            IPAddress     = pFc7Board.config.get("IPAddress",""),
                                            Address_Table = pFc7Board.config.get("Address_Table",""))
        return element


    def createNewPowerSupplyFile( self , pPowerSupply, pOutputFile):
        devices = ET.Element("Devices")
        entry = self.createPowerSupplyEntry( pPowerSupply )
        if entry is not None:
            devices.append( self.createPowerSupplyEntry( pPowerSupply ) )
            try:
                for channel in pPowerSupply.channels:
                    model = pPowerSupply.config["Model"]
                    if model=="CAEN":
                        if channel.config["HVLV"]== "HV":
                            devices[-1].append( self.createCAENHVChannelEntry ( channel, pPowerSupply.config["HVSlot"], pPowerSupply.modelDictionary[model]["HVRange"] ) )
                        if channel.config["HVLV"] == "LV":
                            devices[-1].append( self.createCAENLVChannelEntry ( channel, pPowerSupply.config["LVSlot"], pPowerSupply.modelDictionary[model]["LVRange"] ) )
                    else:
                        devices[-1].append( self.createChannelEntry ( channel ) )
            except AttributeError:
                pass

            #print(ET.tostring(devices, pretty_print=True).decode())
            str=ET.tostring(devices, pretty_print=True)
            with open(pOutputFile, 'wb+') as file:
                file.write(str)

    def createPowerSupplyEntry( self , pPowerSupply ):
        if pPowerSupply.config.get("Connection","") == "Ethernet":
            element = ET.Element("PowerSupply",
                                                InUse       = "Yes",
                                                ID          = pPowerSupply.config.get("ID",""),
                                                Model       = pPowerSupply.config.get("Model","").split("-")[0],
                                                LaunchServer= str(pPowerSupply.config.get("LaunchServer","True")),
                                                ExternalIPPort= pPowerSupply.config.get("ExternalIPPort",""),
                                                Series      = pPowerSupply.config.get("Series",""),
                                                Connection  = pPowerSupply.config.get("Connection",""),
                                                IPAddress   = pPowerSupply.config.get("IPAddress",""),
                                                Timeout     = pPowerSupply.config.get("Timeout",""),
                                                Port        = pPowerSupply.config.get("Port","")  )
        elif pPowerSupply.config.get("Connection","") == "Serial":
            element = ET.Element("PowerSupply",
                                                InUse       = "Yes",
                                                ID          = pPowerSupply.config.get("ID",""),
                                                Model       = pPowerSupply.config.get("Model",""),
                                                LaunchServer= str(pPowerSupply.config.get("LaunchServer","True")),
                                                ExternalIPPort= pPowerSupply.config.get("ExternalIPPort",""),
                                                Series      = pPowerSupply.config.get("Series",""),
                                                Connection  = pPowerSupply.config.get("Connection",""),
                                                Port        = pPowerSupply.config.get("Port",""),
                                                BaudRate    = pPowerSupply.config.get("BaudRate",""),
                                                Timeout     = pPowerSupply.config.get("Timeout",""),
                                                FlowControl = pPowerSupply.config.get("FlowControl",""),
                                                Parity      = pPowerSupply.config.get("Parity",""),
                                                RemoveEcho  = pPowerSupply.config.get("RemoveEcho",""),
                                                Terminator  = pPowerSupply.config.get("Terminator",""),
                                                Suffix      = pPowerSupply.config.get("Suffix",""))
        else:
            element = None

        #Add "special" entries like admin and pw for CAEN
        for key, value in pPowerSupply.modelDictionary[pPowerSupply.config["Model"]].items():
            if key not in ["Channels","HVLV","DefaultPort","Series","LVRange","HVRange"]:
                element.set(key, value)
        if pPowerSupply.config["Model"]=="CAEN":
            element.set("HVSlot", pPowerSupply.config.get("HVSlot",""))
            element.set("LVSlot", pPowerSupply.config.get("LVSlot",""))

        return element

    def createChannelEntry( self, pChannel ):
        #print(pChannel.config)
        element = ET.Element("Channel",
                                        ID      = pChannel.config.get("ID",""),
                                        Channel = pChannel.config.get("Channel","") ,
                                        HVLV    = pChannel.config.get("HVLV","") ,
                                        UNVThr   = pChannel.config.get("UNVThr","") ,
                                        OVVThr   = pChannel.config.get("OVVThr","") ,
                                        InUse   = "Yes" )
        return element

    ## use "0" as standard V0Set if allowed, otherwise take the min abs value in the allowed range
    def getV0fromVRange(self, VRange):
        v0 = 0
        if v0<VRange[0] or v0>VRange[1]:
            if abs(VRange[0])<abs(VRange[1]):
                v0 = VRange[0]
            else:
                v0 = VRange[1]
        return str(v0)

    def createCAENLVChannelEntry( self, pChannel, pLVSlot, VRange):
        element = ET.Element("Channel",
                                        ID      = pChannel.config.get("ID",""),
                                        Channel = pChannel.config.get("Channel",""),
                                        Slot    = pLVSlot,
                                        InUse   = "Yes",
                                        I0Set   ="1.0",
                                        V0Set   = self.getV0fromVRange(VRange),
                                        RUp     ="5",
                                        RDwn    ="5",
                                        Trip    ="0.1",
                                        RUpTime ="100",
                                        RDwTime ="100",
                                        UNVThr  = pChannel.config.get("UNVThr","") ,
                                        OVVThr  = pChannel.config.get("OVVThr",""))
        return element

    def createCAENHVChannelEntry( self, pChannel, pHVSlot, VRange):
        element = ET.Element("Channel",
                                        ID      = pChannel.config.get("ID",""),
                                        Channel = pChannel.config.get("Channel",""),
                                        Slot    = pHVSlot,
                                        InUse   = "Yes",
                                        I0Set   ="100",
                                        V0Set   = self.getV0fromVRange(VRange),
                                        RUp     ="10",
                                        RDwn    ="10",
                                        Trip    ="0.1",
                                        RUpTime ="100",
                                        RDwTime ="100",
                                        UNVThr  = pChannel.config.get("UNVThr","") ,
                                        OVVThr  = pChannel.config.get("OVVThr",""))
        #print(ET.tostring(element, pretty_print=True))
        return element

    def createNewArduinoFile( self , pArduino, pOutputFile):
        devices = ET.Element("Devices")
        devices.append( self.createArduinoEntry( pArduino ) )
        devices[-1].append( self.createKiraConfigEntry( pArduino ) )
        for led in pArduino.leds:
            devices[-1].append(self.createKiraLEDEntry(led))

        #print(ET.tostring(devices, pretty_print=True).decode())
        str=ET.tostring(devices, pretty_print=True)
        with open(pOutputFile, 'w+b') as file:
            file.write(str)

    def createArduinoEntry( self, pArduino ):
        element = ET.Element("Arduino",
                                InUse       = "Yes",
                                ID          = pArduino.config.get("ID",""),
                                Model       = pArduino.config.get("Model",""),
                                LaunchServer= str(pArduino.config.get("LaunchServer","True")),
                                ExternalIPPort= pArduino.config.get("ExternalIPPort",""),
                                Connection  = pArduino.config.get("Connection",""),
                                Port        = pArduino.config.get("Port",""),
                                BaudRate    = pArduino.config.get("BaudRate",""),
                                Timeout     = pArduino.config.get("Timeout",""),
                                FlowControl = pArduino.config.get("FlowControl",""),
                                Parity      = pArduino.config.get("Parity",""),
                                RemoveEcho  = pArduino.config.get("RemoveEcho",""),
                                Terminator  = pArduino.config.get("Terminator",""),
                                Suffix      = pArduino.config.get("Suffix",""))
        return element

    def createKiraConfigEntry( self, pArduino ):
        element = ET.Element("KIRA",
                                Trigger      = pArduino.config.get("Trigger", "on"),
                                TriggerFrequency = pArduino.config.get("TriggerFrequency","1000"),
                                DacLed       = pArduino.config.get("DacLed","low"),
                                PulseLength  = pArduino.config.get("PulseLength","50"))
        return element

    def createKiraLEDEntry( self, pLED ):
        element = ET.Element("LED",
                                ID          = pLED.config.get("ID", ""),
                                Channel     = pLED.config.get("Channel",""),
                                InUse       = pLED.config.get("InUse","Yes"),
                                Intensity   = pLED.config.get("Intensity","0"),
                                Light       = pLED.config.get("Light","off"))
        return element

    def getDevicesFromFile( self, pFile ):
        powerSupplyStatList = []
        arduinoStatList = []
        deviceList = []
        try:
            tree = ET.parse(pFile)
            #print(ET.tostring(tree, pretty_print=True).decode())
            devices = tree.getroot()

            for device in devices:
                if device.tag == "PowerSupply":
                    #print(device.tag)
                    powerSupplyStats       = {}
                    powerSupplyChannels    = []
                    for entries in device.iter():
                        if entries.tag == "PowerSupply" or entries.tag == "Channel":
                            if entries.tag == "PowerSupply":
                                powerSupplyStats["Type"] = "PowerSupply"
                                for name, value in sorted(entries.items()):
                                    #print('%s = %r' % (name, value))
                                    powerSupplyStats[name] = value
                                    #print(name)
                            elif entries.tag == "Channel":
                                channel     = {}
                                for name, value in sorted(entries.items()):
                                    #print('%s = %r' % (name, value))
                                    channel[name] = value
                                powerSupplyChannels.append(channel)
                            powerSupplyStats["Channels"] = powerSupplyChannels
                    deviceList.append(powerSupplyStats)

                elif device.tag == "Arduino":
                    arduinoStats    = {}
                    leds            = []
                    for entries in device.iter():
                        if entries.tag in ["Arduino", "KIRA", "LED"]:
                            if entries.tag in ["Arduino","KIRA"]:
                                arduinoStats["Type"] = "Arduino"
                                for name, value in sorted(entries.items()):
                                    #print('%s = %r' % (name, value))
                                    arduinoStats[name] = value
                                    #print(name)
                            elif entries.tag == "LED":
                                led     = {}
                                for name, value in sorted(entries.items()):
                                    #print('%s = %r' % (name, value))
                                    led[name] = value
                                leds.append(led)
                            arduinoStats["LEDs"] = leds
                    deviceList.append(arduinoStats)
                elif device.tag == "FC7":
                    fc7Stats = {}
                    for entries in device.iter():
                        if entries.tag == "FC7":
                            fc7Stats["Type"] = "FC7"
                            for name, value in sorted(entries.items()):
                                #print('%s = %r' % (name, value))
                                fc7Stats[name] = value
                                #print(name)
                            deviceList.append(fc7Stats)

        except OSError:
            pass
        except ET.XMLSyntaxError:
            pass
        return deviceList

    def saveDevicesToFile( self, pFile, pDeviceList):
        devices = ET.Element("Devices")
        for device in pDeviceList:
            if device.config["Type"] == "FC7":
                devices.append( self.createFc7Entry( device ) )
            elif device.config["Type"] == "PowerSupply":
                devices.append( self.createPowerSupplyEntry( device ) )
                for channel in device.channels:
                    devices[-1].append( self.createChannelEntry ( channel ) )
            elif device.config["Type"] == "Arduino":
                #device.printConfig()
                devices.append( self.createArduinoEntry( device ) )
                devices[-1].append( self.createKiraConfigEntry( device ) )
                for led in device.leds:
                    devices[-1].append(self.createKiraLEDEntry(led))

        #print(ET.tostring(devices, pretty_print=True).decode())
        str=ET.tostring(devices, pretty_print=True)
        with open(pFile, 'w+b') as file:
            file.write(str)
