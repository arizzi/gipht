#------------------------------------------------
#+++++++++++++PowerSupply.py++++++++++++++++++++++
#Created by Stefan Maier            10.07.2020
#Last modified by Stefan Maier      17.08.2020
#------------------------------------------------


from PySide2.QtCore import Signal
from src.Device import Device
from datetime import datetime, timedelta
import yaml

class Channel():
    def __init__( self, pIndex, pStats):
        self.index = pIndex
        self.config = {}
        self.config = pStats
        self.status = {"IsOn": "Unknown", "v_set" : "0", "Voltage": "0", "i_max": "0" , "Current": "0","Timestamp": ""}
        self.monitorData = {"Timestamp": [],
                            "IsOn"   : [],
                            "Voltage": [],
                            "Current": []}
        #print("Channelconfig")
        #print(self.config)
        """
        config = {"Channel": "abc", "ID": "abc", "HVLV": "HV", "Slot": 123, "InUse": "Yes", "OVVThr": x, "UNVThr": y}
        """

    def voltageInRange( self, pVoltage ):
        return float(self.config["UNVThr"]) <= pVoltage <= float(self.config["OVVThr"])

class PowerSupply(Device):

    poll            = Signal ()
    v_max           = 11
    possibleConnections = ["Ethernet", "Serial"]

    def __init__( self, pIndex, pConfig, pPowerSupplyModelDictionary, pDevicePackageFolder):

        self.modelDictionary = pPowerSupplyModelDictionary

        pConfig["Type"] = "PowerSupply"
        #Hardcode some constants to avoid Key errors
        if "Connection" not in pConfig.keys():
            pConfig["Connection"] = "Ethernet"
        if "Model" not in pConfig.keys():
            pConfig["Model"] = list(self.modelDictionary.keys() )[0]
        if "Channels" not in pConfig.keys():
            pConfig["Channels"] = []
        if "Series" not in pConfig.keys():
            pConfig["Series"] = self.modelDictionary[list(self.modelDictionary.keys() )[0]]["Series"][0]
        if "ID" not in pConfig.keys():
            pConfig["ID"] = str(pIndex)+pConfig["Model"]

        self.channels = []

        super(PowerSupply, self).__init__( pIndex, pConfig, pDevicePackageFolder  )

        self.updateXmlFile()
        self.createInterface( self.xmlFile )
        self.changeAndConnectWidget()

    #-----------------------------------------------------------------------
    #---------------------------- Channel handling -------------------------
    #-----------------------------------------------------------------------



    #Generates the Channel objects based on a dictionary generated reading the XML file
    def generateChannels( self, pListOfChannelConfigs ):
        self.channels.clear()
        for config in pListOfChannelConfigs:
            #Decide here wheter the channel is a high or low voltage channel
            self.channels.append (Channel( len(self.channels), config) )

    #Adding a channel can be declined if the channel number is higher than the physically available channels
    #The Channel naming is given by the hardcoded power supply .yaml file
    def getNewChannel( self, pAlreadyListed, pHVLV ):
        model = self.config["Model"]
        lvChannels = self.modelDictionary[model]["Channels"].get("LV",[])
        hvChannels = self.modelDictionary[model]["Channels"].get("HV",[])

        #Check how many channels already have been requested
        if pHVLV == "LV":
            umin = self.modelDictionary[model]["LVRange"][0]
            umax = self.modelDictionary[model]["LVRange"][1]
        elif pHVLV == "HV":
            umin = self.modelDictionary[model]["HVRange"][0]
            umax = self.modelDictionary[model]["HVRange"][1]
        else:
            umin = 0
            umax = 0

        if pAlreadyListed < len(lvChannels) + len(hvChannels):
            newConfig = {   "ID":       pHVLV + self.modelDictionary[model]["Channels"][pHVLV][pAlreadyListed],
                            "Channel":  self.modelDictionary[model]["Channels"][pHVLV][pAlreadyListed], 
                            "HVLV":     pHVLV,
                            "UNVThr":   umin,
                            "OVVThr":   umax
            }
            newChannel = Channel (len(self.channels), newConfig)
        else:
            newChannel = None
        return newChannel

    def remainingChannelsToAssign(self):
        assignedChannels = []
        for channel in self.channels:
            assignedChannels.append(channel.config["Channel"])
        availableChannels = self.modelDictionary[self.config["Model"]]["Channels"]
        return [item for item in availableChannels if item not in assignedChannels]

    #-----------------------------------------------------------------------
    #--------------------------------- Status  -----------------------------
    #-----------------------------------------------------------------------
    def check( self ):
        self.serverOrPipeStatus =  self.hwInterface.running 
        self.readoutPowerSupply()

    def monitor( self , pDumpToFile= None):
        self.readoutPowerSupply()
        self.storeMonitorData = pDumpToFile


    def isHVLV( self ):
        model = self.config.get("Model","")
        ret = ""
        if "HV" in list(self.modelDictionary[model]["Channels"]):
            ret += "HV"
        if "LV" in list(self.modelDictionary[model]["Channels"]):
            ret += "LV"
        return ret


    #-----------------------------------------------------------------------
    #-------------------- Pipe, reader and signal handling -----------------
    #-----------------------------------------------------------------------

    def connectDataInterface( self ):
        self.data.connect(self.managePsData)

    #Data consists of the channelID, the variable and value
    def managePsData(self, pData):
        #print("PS Data:")
        #print( str(self.index) + "\t" + str(pData) )
        #channelId, what, data = pData.split('>')

        #Data of all PS is comming through here, now select the fitting value
        if len(pData.split(',') )> 1:
            timestamp, data = self.decodeStatus( pData )
            timestamp = str( (datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S") + timedelta(seconds=1)).strftime("%Y-%m-%d %H:%M:%S"))
            self.lastReadout = datetime.now()#datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S")
            #print(data)
            if self.status != "ReadingData":
                self.status = "ReadingData"
                self.statusUpdate.emit(self)


            if self.config["ID"] in data.keys():
                for channelInfo in data[self.config["ID"]]:
                    for channel in self.channels:
                        if channelInfo == channel.config["ID"]:
                            channel.status["Timestamp"] = timestamp
                            channel.monitorData["Timestamp"].append(datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S"))
                            log = timestamp + ": " + self.config["ID"] + "(" + channel.config["ID"] + "):   "
                            for measurement in data[self.config["ID"]][channelInfo]:
                                if measurement in channel.monitorData:
                                  channel.status[measurement] = data[self.config["ID"]][channelInfo][measurement]
                                  channel.monitorData[measurement].append( float ( data[self.config["ID"]][channelInfo][measurement] ) )
                                  log += measurement + " : " + data[self.config["ID"]][channelInfo][measurement] + "   "
                            self.logUpdate.emit( log )
                if self.storeMonitorData is not None:
                    with open ( self.storeMonitorData, 'a') as f:
                        yaml.dump([{"Timestamp": timestamp, "Channels": data[self.config["ID"]] }],f)


            else:
                print("GIPHT:\tPower supply data (" + self.config["ID"]+"): " + str ( data[self.config["ID"]] ) )

        else:
            #print("repoll")
            self.readoutPowerSupply()



    #-----------------------------------------------------------------------
    #-------- Communication with power supply via the ph2 acf---------------
    #-----------------------------------------------------------------------

    #This section contains the different set methods for the powersupply
    #After a settting change, always wait 500 ms and then poll the power supply once to check the settings

    #Gives the readout command to the Ph2_ACF
    def readoutPowerSupply( self ):
        if self.hwInterface.running:
            print( "GIPHT:\tPower supply (" + self.config["ID"] + ") - Readout")
            cmd = "GetStatus,PowerSupplyId:" + self.config["ID"]
            self.task = "Readout power supply"
            self.execute(cmd, True)

    def enableChannel ( self, pChannelIndex ):
        if self.channels[pChannelIndex].status["IsOn"] == "0" or self.channels[pChannelIndex].status["IsOn"] == "Unknown":
            print( "GIPHT:\tPower supply (" + self.config["ID"] + ") - Enable channel " + self.channels[pChannelIndex].config["ID"])
            cmd = "TurnOn,PowerSupplyId:" + self.config["ID"] + ",ChannelId:" + self.channels[pChannelIndex].config["ID"] 
            self.task = "Enable channel"
            self.execute(cmd, True)
        else:
            self.readoutPowerSupply()

    def disableChannel ( self, pChannelIndex ):
        if self.channels[pChannelIndex].status["IsOn"] == "1" or self.channels[pChannelIndex].status["IsOn"] == "Unknown":
            print( "GIPHT:\tPower supply (" + self.config["ID"] + ") - Disable channel " + self.channels[pChannelIndex].config["ID"])
            cmd = "TurnOff,PowerSupplyId:" + self.config["ID"] + ",ChannelId:" + self.channels[pChannelIndex].config["ID"] 
            self.task = "Disable channel"
            self.execute(cmd, True)
        else:
            self.readoutPowerSupply()

    #Switch the channel state. If it is unknown( by startup) set in off
    def enableDisableChannel( self , pChannelIndex ):
        if self.channels[pChannelIndex].status["IsOn"] == "1" or self.channels[pChannelIndex].status["IsOn"] == "Unknown":
            print( "GIPHT:\tPower supply (" + self.config["ID"] + ") - Disable channel " + self.channels[pChannelIndex].config["ID"])
            cmd = "TurnOff,PowerSupplyId:" + self.config["ID"] + ",ChannelId:" + self.channels[pChannelIndex].config["ID"] 
        elif self.channels[pChannelIndex].status["IsOn"] == "0":
            print( "GIPHT:\tPower supply (" + self.config["ID"] + ") - Enable channel " + self.channels[pChannelIndex].config["ID"])
            cmd = "TurnOn,PowerSupplyId:" + self.config["ID"] + ",ChannelId:" + self.channels[pChannelIndex].config["ID"] 
        self.task = "Enable or disable channel voltage"
        self.execute(cmd, True)

    def setVoltage( self, pChannelIndex, pVoltage ):
        print( "GIPHT:\tPower supply (" + self.config["ID"] + ") - Set voltage on channel " + self.channels[pChannelIndex].config["ID"] + " to " + str(pVoltage))
        cmd = "SetVoltage,Voltage:" + str(pVoltage)+ ",PowerSupplyId:" + self.config["ID"] + ",ChannelId:" + self.channels[pChannelIndex].config["ID"] 
        self.task = "Set voltage"
        self.execute(cmd, True)
    
    def setMaximumCurrent( self, pChannelIndex, pMaximumCurrent ):
        print("TO BE IMPLEMENTED: " + str(pMaximumCurrent))

