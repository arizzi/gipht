# Gipht

Graphical user Interface for PHase 2 Tracker objects v3

[Here](https://gitlab.cern.ch/smaier/ph2_ot_module_test_manual/-/blob/master/manual.pdf you can find the current manual for the Outer Tracker module test bench and GIPHT.

Installation
```bash
git clone --recurse-submodules -b master https://gitlab.cern.ch/cms_tk_ph2/gipht.git
cd gipht
sh compileSubModules.sh
```
Update
```bash
git stash
git pull --recurse-submodules
git submodule update --recursive --remote
sh compileSubModules.sh
```
#git submodule update --remote
GIPHT requirements:
For developers:
1. `sudo dnf groupinstall "Development Tools"`
2. `sudo dnf install qtcreator`

For Users:
1. `sudo dnf install python3`
2. `sudo python3 -m pip install -U pip`
3. `sudo python3 -m pip install -r requirements.txt`

If not done yet add your user to the dialout group to have access to the serial ports:
```bash
sudo usermod -a -G dialout $USER
```

Start
```bash
python3 gipht.py
```



### Running Gipht in a docker container

1. Find a docker image in the container registry of this repo. https://gitlab.cern.ch/cms_tk_ph2/gipht/container_registry

1. On your docker host (tested on a linux host w/ CS8) run:

    ```bash
    SOCK=/tmp/.X11-unix; XAUTH=/tmp/.docker.xauth; xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -; chmod 777 $XAUTH;
    ```

1. To get all connected ports:
    ```bash
    python3 -m serial.tools.list_ports -q
    ```

1. And then start the docker container from one of the Gipht images using the following options:

    ```bash
    docker run --rm -ti -v $PWD:$PWD -w $PWD -e DISPLAY=$DISPLAY -v $XSOCK:$XSOCK -v $XAUTH:$XAUTH   -e XAUTHORITY=$XAUTH --net host \
           --device=ttyACM0 --device=ttyUSB0 \ #from connected ports
           gitlab-registry.cern.ch/cms_tk_ph2/gipht/docker:latest
    ```

You can also put all commands in a single script, `runDocker.sh`:
```bash
#!/bin/bash
SOCK=/tmp/.X11-unix; XAUTH=/tmp/.docker.xauth; xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -; chmod 777 $XAUTH;

output=$(python3 -m serial.tools.list_ports -q)
IFS=$'\n' ports=($output)
devices=""
for port in "${ports[@]}"; do
    if [ ! -z "$port" ]; then
        devices+="--device=$port "
    fi
done
devices=$(echo $devices | xargs)

docker run --rm -ti -v $PWD:$PWD -w $PWD -e DISPLAY=$DISPLAY -v $XSOCK:$XSOCK -v $XAUTH:$XAUTH   -e XAUTHORITY=$XAUTH --net host $devices gitlab-registry.cern.ch/cms_tk_ph2/gipht/docker:latest
```

To update the docker image, just run `docker pull gitlab-registry.cern.ch/cms_tk_ph2/gipht/docker:latest`