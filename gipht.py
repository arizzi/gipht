import sys, signal

from PySide2.QtWidgets import QApplication
from src.MainWindow import MainWindow
from src.GuiController import GuiController

if __name__ == "__main__":
    app = QApplication(sys.argv)
    signal.signal(signal.SIGINT,signal.SIG_DFL)
    window = MainWindow( )
    controller = GuiController( window )
    controller.start()
    window.show()
    window.setWindowTitle("Graphical user Interface for PHase 2 Tracker objects - GIPHT v3.11")
    sys.exit(app.exec_())